# -*- coding: utf-8 -*-
"""
Created on Mon Oct 21 16:30:03 2013

@author: oreichri
"""

from PyQt4.QtCore import *
from PyQt4.QtGui import *

import pandas as pd
import datetime
import numpy as np
        

Qt.EditNoEmit = Qt.UserRole + 1


class TableModel(QAbstractTableModel): 
    def __init__(self, parent=None, *args): 
        """ datain: a list of lists
            headerdata: a list of strings
        """
        QAbstractTableModel.__init__(self, parent, *args) 
        
        self.modelData   = pd.DataFrame()     
        self.showCol     = {}
        self.visibleCols = []
        
    def rowCount(self, parent=None): 
        return self.modelData.shape[0]
 
    def columnCount(self, parent=None): 
        return sum(self.showCol.values())
 
 
    def data(self, index, role):
        if self.columnCount() == 0 or not index.isValid():
            return None       
        
        if role != Qt.DisplayRole: 
            return None
            
        return self.modelData.ix[index.row(), self.visibleCols[index.column()]] 



    def headerData(self, col, orientation, role):
        colName = self.visibleCols[col]
        if orientation == Qt.Horizontal and role == Qt.DisplayRole and self.showCol[colName]:
            return colName
        return None


    def sort(self, Ncol, order):
        """Sort table by given column number.
        """
        if self.columnCount() == 0:
            return

        #self.layoutAboutToBeChanged.emit()
        
        if order == Qt.DescendingOrder:
            ascending  = False           
        else:
            ascending  = True            
            
        self.modelData.sort(self.visibleCols[Ncol], ascending=ascending, inplace=True)
        self.modelData.reset_index(drop=True, inplace=True)            
            
        topLeft     = self.createIndex(0, 0) 
        bottomRight = self.createIndex(self.rowCount()-1, self.columnCount()-1)
        self.dataChanged.emit(topLeft, bottomRight)

        #self.layoutChanged.emit()
        
        
        

###############################################################################
# Events
###############################################################################
        

class EventTableModel(TableModel): 

    def setTableData(self, events, recordingStartTime):
        deltaTimes      = [e.startTime for e in events]
        startTimes      = [(recordingStartTime + datetime.timedelta(0, d)).strftime("%H:%M:%S") for d in deltaTimes]
        eventGroupNames = [e.groupName for e in events]
        eventTypes      = [e.name      for e in events]
        channels        = [e.channel   for e in events]
        self.layoutAboutToBeChanged.emit()        
        self.modelData = pd.DataFrame({"group"     : eventGroupNames, 
                                  "type"      : eventTypes,
                                  "duration"  : [str(e.duration())   for e in events],
                                  "channel"   : channels,
                                  "time start": startTimes,
                                  "delta time": deltaTimes})   
                      
        self.showCol = {"group"     : True, 
                        "type"      : True,
                        "duration"  : True,
                        "time start": True,
                        "channel"   : True,
                        "delta time": False}   
 
        self.visibleCols = [name for name in self.modelData.columns.tolist() if self.showCol[name]]                            

        self.layoutChanged.emit()

        
    def getRowTime(self, noRow):
        return self.modelData.ix[noRow, "delta time"]
        


import os

def makeIconButton(imagePath):
    button              = QPushButton()
    pixmap              = QPixmap(imagePath)
    button.setIcon(QIcon(pixmap))
    button.setIconSize(pixmap.rect().size())  
    button.setGeometry(pixmap.rect())
    button.setSizePolicy(QSizePolicy.Minimum, QSizePolicy.Fixed)   
    return button


class EventWidget(QWidget):
    
    def __init__(self, mainWindow, parent = None):
        super(EventWidget, self).__init__(parent)
        self.mainWindow = mainWindow

        self.setLayout(QVBoxLayout())  
        
        self.createTable() 
        
        self.headers = self.eventTableView.horizontalHeader()
        self.headers.setContextMenuPolicy(Qt.CustomContextMenu)
        self.headers.customContextMenuRequested.connect(self.header_popup)        
        

        self.layout().addWidget(self.eventTableView) 

        self.eventTableView.doubleClicked.connect(self.tableDoubleClicked)


    def tableDoubleClicked(self, indexObj):
        rowTime = self.eventTableModel.getRowTime(indexObj.row())
        self.mainWindow.goToTimeDelta(rowTime)


        
        
    def header_popup(self, pos):
        menu = QMenu()
        quitAction = menu.addAction("Quit")
        action = menu.exec_(self.mapToGlobal(pos))
        if action == quitAction:
            qApp.quit()        
        

    def popup(self, pos):
        for i in self.tv.selectionModel().selection().indexes():
            print i.row(), i.column()
        menu = QMenu()
        quitAction = menu.addAction("Quit")
        action = menu.exec_(self.mapToGlobal(pos))
        if action == quitAction:
            qApp.quit()
        
        
        
    def setEventData(self, events, recordingStartTime):
        self.eventTableModel.setTableData(events, recordingStartTime)

    def createTable(self):
        # create the view
        self.eventTableView = QTableView()

        # set the table model
        self.eventTableModel = EventTableModel(self) 
        self.eventTableView.setModel(self.eventTableModel)

        # set the minimum size
        self.eventTableView.setMinimumSize(400, 300)

        # hide grid
        self.eventTableView.setShowGrid(False)

        # set the font
        font = QFont("Courier New", 8)
        self.eventTableView.setFont(font)

        # hide vertical header
        vh = self.eventTableView.verticalHeader()
        vh.setVisible(False)

        # set horizontal header properties
        hh = self.eventTableView.horizontalHeader()
        hh.setStretchLastSection(True)

        # set column width to fit contents
        self.eventTableView.resizeColumnsToContents()

        # set row height
        for row in xrange(self.eventTableModel.rowCount()):
            self.eventTableView.setRowHeight(row, 18)

        # enable sorting
        self.eventTableView.setSortingEnabled(True)


        











###############################################################################
# Class for selecting event groups and types
###############################################################################
        

class UseSelectionWidget(QWidget):
    

    #useChanged = Signal(str, bool) 
    useChangedAll = Signal(dict)    
    
    def __init__(self, mainWindow, parent = None):
        super(UseSelectionWidget, self).__init__(parent)
        self.mainWindow = mainWindow

        self.setLayout(QVBoxLayout())  
        
        self.createTable() 
        
        self.headers = self.eventTableView.horizontalHeader()
        self.headers.setContextMenuPolicy(Qt.CustomContextMenu)
        self.headers.customContextMenuRequested.connect(self.header_popup)        
        
        self.eventTableView.setContextMenuPolicy(Qt.CustomContextMenu)
        self.eventTableView.customContextMenuRequested.connect(self.popup)        
        

        self.buttonWidget   = QWidget()
        self.buttonWidget.setLayout(QHBoxLayout())
        
        self.checkAllBtn    = makeIconButton(os.path.join(os.path.dirname(__file__), "images", "checkAll.png"))
        self.uncheckAllBtn  = makeIconButton(os.path.join(os.path.dirname(__file__), "images", "uncheckAll.png"))
        self.filterBtn      = makeIconButton(os.path.join(os.path.dirname(__file__), "images", "filter.png"))
        
        
        self.checkAllBtn.clicked.connect(self.checkAll)
        self.uncheckAllBtn.clicked.connect(self.uncheckAll)
        self.filterBtn.clicked.connect(self.filter)
        

        self.buttonWidget.layout().addWidget(self.checkAllBtn)   
        self.buttonWidget.layout().addWidget(self.uncheckAllBtn)  
        self.buttonWidget.layout().addWidget(self.filterBtn)    
        self.buttonWidget.layout().addStretch()       
        

        self.layout().addWidget(self.eventTableView) 
        self.layout().addWidget(self.buttonWidget)

        

    def checkAll(self):    
        self.eventTableModel.checkAll()
        
    def uncheckAll(self):   
        self.eventTableModel.uncheckAll()
        
        
    def filter(self):  
        names = self.eventTableModel.modelData["name"]
        uses  = self.eventTableModel.modelData["use"]    
        self.useChangedAll.emit(dict((name, use) for use, name in zip(uses, names)))
            
        
        
        
    def header_popup(self, pos):
        menu = QMenu()
        quitAction = menu.addAction("Quit")
        action = menu.exec_(self.mapToGlobal(pos))
        if action == quitAction:
            qApp.quit()        
        

    def popup(self, pos):
        menu = QMenu()
        setColorAction = menu.addAction("Set color")
        action = menu.exec_(self.mapToGlobal(pos))
        if action == setColorAction:
            row = self.eventTableView.indexAt(pos).row()
            if row != -1:
                color = QColorDialog.getColor(QColor("black"), self)
                eventName = self.eventTableModel.modelData.ix[row, "name"]        
                self.mainWindow.eventColorDict[eventName] =  color               
                print color, eventName                
                
        
        
        
    def setTableData(self, eventGroups):
        self.eventTableModel.setTableData(eventGroups)    
        self.eventTableView.setDelegates(self.eventTableModel)

    def createTable(self):
        # create the view
        self.eventTableView = TableView()

        # set the table model
        self.eventTableModel = UseTableModel(self) 
        #self.eventTableModel.useChanged.connect(self.changeEventTypeDisplay)        
        
        self.eventTableView.setModel(self.eventTableModel)

        # set the minimum size
        self.eventTableView.setMinimumSize(400, 300)

        # hide grid
        self.eventTableView.setShowGrid(False)

        # set the font
        font = QFont("Courier New", 8)
        self.eventTableView.setFont(font)

        # hide vertical header
        vh = self.eventTableView.verticalHeader()
        vh.setVisible(False)

        # set horizontal header properties
        hh = self.eventTableView.horizontalHeader()
        hh.setStretchLastSection(True)

        # set column width to fit contents
        self.eventTableView.resizeColumnsToContents()

        # set row height
        for row in xrange(self.eventTableModel.rowCount()):
            self.eventTableView.setRowHeight(row, 18)

        # enable sorting
        self.eventTableView.setSortingEnabled(True)


    #def changeEventTypeDisplay(self, name, display):     
    #    self.useChanged.emit(name, display)









class UseTableModel(TableModel): 
    

    useChanged    = Signal(str, bool)
    useChangedAll = Signal(str, bool)
    
    def setTableData(self, data):    
        
        self.layoutAboutToBeChanged.emit()            
        
        self.modelData = pd.DataFrame({"name" : data.keys() ,
                                       "use"  : data.values()}   ) 
        self.showCol = {"name" : True,
                        "use"  : True}      
        self.typeDel = {"name" : "text", 
                        "use"  : "checkbox"}
            
        self.modelData.sort("name", inplace=True)
        self.modelData.reset_index(drop=True, inplace=True)           
                                    
        self.visibleCols = [name for name in self.modelData.columns.tolist() if self.showCol[name]]                           
                        
        self.layoutChanged.emit()


    def checkAll(self, checked=True):    
        if self.columnCount():      
            self.modelData["use"] = [checked]*len(self.modelData["use"])   
            col         = self.visibleCols.index("use")
            topLeft     = self.createIndex(0, col) 
            bottomRight = self.createIndex(self.rowCount()-1, col)
            self.dataChanged.emit(topLeft, bottomRight)
    
    def uncheckAll(self):    
        self.checkAll(False)    


    def flags(self, index):
        colName = self.modelData.columns[index.column()]
        if colName == "use":   
            return Qt.ItemIsEditable | Qt.ItemIsEnabled
        else:
            return Qt.ItemIsEnabled


    def setData(self, index, value, role):
       
        colName = self.modelData.columns[index.column()]
        if self.typeDel[colName] == "checkbox":
            if role == Qt.EditRole:
                self.modelData.ix[index.row(), colName] = value
                self.dataChanged.emit(index, index)  
            elif role == Qt.EditNoEmit:
                self.modelData.ix[index.row(), colName] = value
            else:
                return False
                          
            
            #if colName == "use" and role == Qt.EditRole:
            #    self.useChanged.emit(self.modelData.ix[index.row(), "name"], value)
            
            return True
        return False


    def data(self, index, role): 
        if not index.isValid() or self.columnCount() == 0:
            return None
            
        if not self.showCol[self.modelData.columns[index.column()]]:
            return None     

        if role == Qt.EditRole: 
            checked = Qt.Checked if self.modelData.iloc[index.row(), index.column()] else Qt.Unchecked
            return checked  
        
        if role == Qt.DisplayRole: 
            if self.typeDel[self.modelData.columns[index.column()]] != "checkbox":      
                return str(self.modelData.iloc[index.row(), index.column()]) 

        return None





       
class TableView(QTableView):

    def __init__(self, *args, **kwargs):
        QTableView.__init__(self, *args, **kwargs)
        
        self.setEditTriggers(QAbstractItemView.AllEditTriggers)

        
    def setDelegates(self, model):
        for i, colname in enumerate(model.modelData.columns) :
            if model.typeDel[colname] == "checkbox":
                self.setItemDelegateForColumn(i, CheckBoxDelegate(self))
                for row in range(model.rowCount()):
                    self.openPersistentEditor(model.index(row, i))




class DVCheckBox(QCheckBox):

    customStateChanged = Signal(int, QModelIndex)
    customClicked      = Signal(int, QModelIndex)

    def __init__(self, parent, index):
        QCheckBox.__init__(self, parent)
        
        self.index = index    
        
        #self.stateChanged.connect(self.relaunchStateChanged)
        self.clicked.connect(self.relaunchClicked)
        
    #def relaunchStateChanged(self, state):
    #    self.customStateChanged.emit(state, self.index)
        
    def relaunchClicked(self, state):  
        self.customClicked.emit(self.checkState(), self.index)






class CheckBoxDelegate(QStyledItemDelegate):
    def __init__(self, parent):
        QStyledItemDelegate.__init__(self, parent)

    def createEditor(self, parent, option, index):
        checkbox = DVCheckBox(parent, index)    
        checkbox.setChecked(Qt.Checked)
        checkbox.customClicked.connect(self.checkboxCustomClicked)
        return checkbox


    def checkboxCustomClicked(self, state, index):
        value = state == Qt.Checked     
        index.model().setData(index, value, Qt.EditNoEmit)
        
    def editorEvent(event, model, option, index):
        return True
        
    def setEditorData(self, editor, index):
        #blocked = editor.signalsBlocked()
        #editor.blockSignals(True)
        editor.setChecked(index.model().data(index, Qt.EditRole))
        #editor.blockSignals(blocked)        
        
        
    def setModelData(self, editor, model, index):
        pass
        

        #QStyledItemDelegate.setEditorData(self, editor, index)
   
   
"""
     int value = index.model()->data(index, Qt::EditRole).toInt();

     QSpinBox *spinBox = static_cast<QSpinBox*>(editor);
     spinBox->setValue(value);
 }
"""
        