# -*- coding: utf-8 -*-

#!/usr/bin/env python

# for debugging, requires: python configure.py  --trace ...
if False:
    import sip
    sip.settracemask(0x3f)

from PyQt4.QtCore import *
from PyQt4.QtGui import *
from copy import deepcopy

import sys, os
import cPickle
import datetime
import numpy as np


#from FIRfilter import FIRFilter_PassBand
from spyndle.io import EDFReader, HarmonieReader
from spyndle.miscellaneous import channelType
from spyndle.miscellaneous import cycleDefinitions

from optionDlg import setSignalsDlg, setFiltersDlg, showHypnogramToolDlg, showSpingleReportDlg
from TimeFrequency import showSpectogramToolDlg
from drawingTools import MultiCanalDataPlot
from sigDisplayInfo import SigDisplayInfo
from eventWidgets import EventWidget, UseSelectionWidget





#import PyGFT


class PersistenceClass:
    def __init__(self) :
        self.sigPath = "c:/"
        
   
        

class MainWindow(QMainWindow):

    def __init__(self, *args):
        apply(QMainWindow.__init__, (self, ) + args)
        self.setWindowTitle('Polysomnographic viewer') 

        self.mainWidget = QWidget(self)

        self.setCentralWidget (self.mainWidget)

        self.setSignalBtn = QPushButton("Set signals")
        
        
        self.timeLbl      = QLabel()
        self.pageSlider   = QSlider(Qt.Horizontal)

        self.sliderLayout = QHBoxLayout()
        self.sliderLayout.addWidget(self.timeLbl)
        self.sliderLayout.addWidget(self.pageSlider )
 
        self.mainLayout     = QGridLayout(self.mainWidget)

        self.polysomPlotter = PolysomPlotter(self)
    
        self.mainLayout.addWidget(self.polysomPlotter, 1, 1)
        self.mainLayout.addLayout(self.sliderLayout, 2, 1)
        self.mainLayout.addWidget(self.setSignalBtn, 3, 1)

        #self.pageDuration = 30.0 # en secondes        
        self.displayDuration = 31.0 # en secondes        

        self.reader  = None
        self.SigDisplayInf = SigDisplayInfo()

        self.filterBank = []



        self.eventWidget        = EventWidget(self)
        self.eventGroupWidget   = UseSelectionWidget(self)
        self.eventTypeWidget    = UseSelectionWidget(self)


        self.eventGroupWidget.useChangedAll.connect(self.eventGroupUseChangedAll)
        self.eventTypeWidget.useChangedAll.connect(self.eventTypeUseChangedAll)





        self.docks =  {"Events":QDockWidget("Events", self),
                       "Event groups":QDockWidget("Event groups", self),
                       "Event types":QDockWidget("Event types", self)}
        
        self.docks["Events"].setWidget(self.eventWidget)   
        self.docks["Event groups"].setWidget(self.eventGroupWidget)   
        self.docks["Event types"].setWidget(self.eventTypeWidget)   
        
        self.addDockWidget(Qt.RightDockWidgetArea, self.docks["Events"])
        self.addDockWidget(Qt.RightDockWidgetArea, self.docks["Event groups"])
        self.addDockWidget(Qt.RightDockWidgetArea, self.docks["Event types"])
        self.tabifyDockWidget(self.docks["Event groups"], self.docks["Event types"]);
        
 
        if os.path.exists("sleepCycleDefClass.pck"):        
            self.sleepCycleDef = cPickle.load(file("sleepCycleDefClass.pck", "r"))               
        else:
            self.sleepCycleDef = cycleDefinitions()

        if os.path.exists("persistanceClass.pck"):    
            try:
                self.persistenceClass = cPickle.load(file("persistanceClass.pck", "r"))     
                
            # Attribute errors may hapen when changing the code such that the persistent object
            # are not the same anymore.
            except AttributeError:
                self.persistenceClass =  PersistenceClass()
        else:
            self.persistenceClass =  PersistenceClass()






        self.statusBar().showMessage('')

        self.setSignalBtn.clicked.connect(self.setSignalBtnClicked)
        
        
        self.pageSlider.sliderReleased.connect(self.sliderReleased)        
        
        #self.pageSlider.setTracking(False)


        self.showMaximized ()
        #self.showFullScreen()        self.plottingArea = MultiCanalDataPlot()


        self.upKeySC = QShortcut(QKeySequence(Qt.Key_Up), self)
        self.upKeySC.activated.connect(self.upKeyPressed)
        
        self.downKeySC = QShortcut(QKeySequence(Qt.Key_Down), self)
        self.downKeySC.activated.connect(self.downKeyPressed)
        
        self.leftKeySC = QShortcut(QKeySequence(Qt.Key_Left), self)
        self.leftKeySC.activated.connect(self.leftKeyPressed)
        
        self.rightKeySC = QShortcut(QKeySequence(Qt.Key_Right), self)
        self.rightKeySC.activated.connect(self.rightKeyPressed)




        # File Menu
        openAction = QAction(QIcon('open.png'), 'Open', self)        
        openAction.setShortcut('Ctrl+O')
        openAction.setStatusTip('Open a data file')
        openAction.triggered.connect(self.openDataFile)

        exitAction = QAction(QIcon('exit.png'), '&Exit', self)        
        exitAction.setShortcut('Ctrl+Q')
        exitAction.setStatusTip('Exit application')
        exitAction.triggered.connect(qApp.quit)

        menubar = self.menuBar()
        fileMenu = menubar.addMenu('&File')
        fileMenu.addAction(openAction)
        fileMenu.addAction(exitAction)




        # Option Menu
        filterAction = QAction('&Filters...', self)        
        filterAction.setShortcut('Alt+F')
        filterAction.setStatusTip('Setup the data filters.')
        filterAction.triggered.connect(self.showFilterDlg)

        setSignalsAction = QAction('Set &signals', self)        
        setSignalsAction.setShortcut('Alt+S')
        setSignalsAction.setStatusTip('Set the signal display.')
        setSignalsAction.triggered.connect(self.setSignalBtnClicked)

        menubar = self.menuBar()
        optionMenu = menubar.addMenu('&Options')
        optionMenu.addAction(filterAction)
        optionMenu.addAction(setSignalsAction)




        # Tools Menu
        hypnogramAction = QAction('&Hypnogram', self)        
        hypnogramAction.setShortcut('Alt+H')
        hypnogramAction.setStatusTip('Open an hypnogram plotter tool.')
        hypnogramAction.triggered.connect(self.showHypnogramDlg)

        spectogramAction = QAction('&Spectogram', self)        
        #hypnogramAction.setShortcut('Alt+H')
        spectogramAction.setStatusTip('Open an spectogram plotter tool.')
        spectogramAction.triggered.connect(self.showSpectogramDlg)

        resyncAction = QAction('&Resynchronize events', self)        
        #hypnogramAction.setShortcut('Alt+H')
        resyncAction.setStatusTip('Resynchronize files with problematic recording gaps.')
        resyncAction.triggered.connect(self.resyncEvents)



        menubar = self.menuBar()
        toolsMenu = menubar.addMenu('&Tools')
        toolsMenu.addAction(hypnogramAction)
        toolsMenu.addAction(spectogramAction)
        toolsMenu.addAction(resyncAction)





        # Reports Menu
        spindleAction = QAction('&Spindle', self)        
        spindleAction.setShortcut('Alt+N')
        spindleAction.setStatusTip('Generate a report on spindles.')
        spindleAction.triggered.connect(self.generateSpindleReportDlg)

        menubar = self.menuBar()
        reportsMenu = menubar.addMenu('&Reports')
        reportsMenu.addAction(spindleAction)


    def generateSpindleReportDlg(self):

        if self.reader is None:
            errMessage = QErrorMessage(self)            
            errMessage.showMessage(QString("Error: " + "You first need to open a data file."))
            return

        dlg = showSpingleReportDlg(self)
        dlg.exec_()
        
        
    def showSpectogramDlg(self):
        
        if self.reader is None:
            errMessage = QErrorMessage(self)            
            errMessage.showMessage(QString("Error: " + "You first need to open a data file."))
            return 

        #stageEventLst = filter(lambda e: e.groupName == "Stage", self.reader.events)     

        QTime.fromString(self.timeLbl.text(), "hh:mm:ss")
        dlg = showSpectogramToolDlg(self) #, QTime.fromString(self.timeLbl.text(), "hh:mm:ss")) #, stageEventLst, self.sleepCycleDef)
        dlg.exec_()
        

    def showHypnogramDlg(self):

        if self.reader is None:
            errMessage = QErrorMessage(self)            
            errMessage.showMessage(QString("Error: " + "You first need to open a data file."))
            return 

        stageEventLst = filter(lambda e: e.groupName == "Stage", self.reader.events)     

        dlg = showHypnogramToolDlg(self, stageEventLst, self.sleepCycleDef)
        dlg.exec_()
        
        
        

    def showFilterDlg(self):

        if self.reader is None:
            errMessage = QErrorMessage(self)            
            errMessage.showMessage(QString("Error: " + "You first need to open a data file."))
            return        
        
        sampRateDict = {}
        for key in channelType:
            samplingRate = choose(nonzero(array(self.reader.getChannelTypes()) == channelType[key]), self.reader.getSamplingRates())[0]
            if len(unique(samplingRate)) != 1 :
                print "Error: Different sampling rates for a same channel type."
                print key, samplingRate
            else:
                sampRateDict[key] = unique(samplingRate)[0]

        dlg = setFiltersDlg(sampRateDict, self)
        dlg.exec_()
        self.updateSignalsPerPage()




    def goToTimeDelta(self, timeDelta):
        if timeDelta < 0.0 :
            raise ValueError("A negative timeDelta value (" + str(timeDelta) + 
                             ") has been has been passed to the goTimeDelta "\
                             "method. Only positive or nul values are accepted.")
        
        inds = np.where(self.pageTimeStart <= timeDelta)[0]
        if len(inds) == 0 :
            self.currentPage = 1 
        else:
            self.currentPage = inds[-1] + 1
            
        self.pageOffset  = timeDelta - self.pageTimeStart[self.currentPage-1]
        self.updateSignalsPerPage()
        self.pageSlider.setValue(self.pageTimeStart[self.currentPage-1])



    def upKeyPressed(self):
        self.goPreviousPage()     
        
    def downKeyPressed(self):   
        self.goNextPage()


    def goNextPage(self):
        if self.pageOffset < 0 :
            self.pageOffset  = 0
            #self.currentPage = 1                   
        elif self.currentPage < len(self.pageTimeStart) :
            self.pageOffset = 0
            self.currentPage += 1
        else:
            return
            
        self.updateSignalsPerPage()
        self.pageSlider.setValue(self.pageTimeStart[self.currentPage-1])

    def goPreviousPage(self):
        if(self.currentPage > 1):
            self.pageOffset = 0
            self.currentPage -= 1
            self.updateSignalsPerPage()
            self.pageSlider.setValue(self.pageTimeStart[self.currentPage-1])


    def rightKeyPressed(self):
        self.pageOffset += 2.0
        self.updateSignalsPerPage()
        self.pageSlider.setValue(self.pageTimeStart[self.currentPage-1] + self.pageOffset)        

    def leftKeyPressed(self):
        self.pageOffset -= 2.0
        self.updateSignalsPerPage()
        self.pageSlider.setValue(self.pageTimeStart[self.currentPage-1] + self.pageOffset)   
        

    def pageChangeRequested(self, noPage):
        self.currentPage = noPage
        self.pageOffset = 0
        self.updateSignalsPerPage()



    def sliderReleased(self):
        #print "slider release"
        sliderPosition = self.pageSlider.value()
        if sliderPosition < self.pageTimeStart[0] :
            self.currentPage = 1
        elif sliderPosition > self.pageTimeStart[-1] :
            self.currentPage = len(self.pageTimeStart)
        else:
            self.currentPage = np.where(sliderPosition >= np.array(self.pageTimeStart))[0][-1]+1
           
        self.pageOffset = sliderPosition - self.pageTimeStart[self.currentPage-1]
        self.updateSignalsPerPage()
        

    



    def updateSignalsPerPage(self):
        
        signalNames = [str(record.name) for record in self.SigDisplayInf.records]          
        if len(signalNames) :       

            print self.pageTimeStart[self.currentPage-1],  self.pageOffset
            start = self.pageTimeStart[self.currentPage-1] + self.pageOffset

            try:
                signalsData = self.reader.read(signalNames, start, self.displayDuration)           
            except:
                print "signalNames: ", signalNames
                print "start: ", start
                print "self.displayDuration: ", self.displayDuration
                print self.pageTimeStart[self.currentPage-1], self.pageOffset, self.currentPage-1
                raise
            
            stop = start + self.displayDuration
            
            filteredData = self.applyFilters(signalsData)
            #print "Update polysomPlotter"
            self.polysomPlotter.updateSignals(self.SigDisplayInf, filteredData, self.reader, self.displayedEvents, start, stop)
            
            self.timeLbl.setText(self.relativeTimeToAbsoluteTime(start).strftime("%H:%M:%S"))


    def relativeTimeToAbsoluteTime(self, relativeTime):
        return self.reader.getRecordingStartTime() + datetime.timedelta(0, relativeTime) # days, seconds, then other fields.
        
        #hour  = int(relativeTime/3600.0)%24           
        #minute = int((relativeTime - int(relativeTime/3600.0)*3600.0)/60.0)%60   
        #seconde = int(relativeTime - int(relativeTime/3600.0)*3600.0 - minute*60.0)       
        #return datetime.time(hour, minute, seconde)
        
        
        
        

    def applyFilters(self, signalsData):
        
        returnData = deepcopy(signalsData)
        for channel in returnData :            
            filtersToApply = filter(lambda e: e.channelType == returnData[channel].type, self.filterBank)

            for dataFilter in filtersToApply:
                returnData[channel].signal = dataFilter.applyFilter(returnData[channel].signal)

        return returnData




    def setDisplayedSignals(self):
        self.polysomPlotter.setSignals(self.SigDisplayInf)


    @property
    def eventColorDict(self):
        return self.polysomPlotter.plottingArea.eventColors  

    @eventColorDict.setter
    def eventColorDict(self, colDict):
        self.polysomPlotter.plottingArea.eventColors = colDict 
        
        # TODO: Could just change the color of the event boxes and replot().
        # TODO: Does not refresh immediatelly as it should...
        self.updateSignalsPerPage()
        
        

    def resyncEvents(self):
        # For files recorderd with an old version of Harmonie, recordings gaps
        # create a disparity between startTime and startSample of the events.
        # This function resynchronize the startTimes.
        self.reader.resyncEvents()
        self.pageTimeStart, self.pageDatetimeStart = self.reader.getPages()
        self.updateSignalsPerPage()
        
        
                
                
    def openDataFile(self):
        fname = QFileDialog.getOpenFileName(self, 'Open a data file', self.persistenceClass.sigPath)
                
        self.persistenceClass.sigPath = os.path.split(str(fname))[0]   
        cPickle.dump(self.persistenceClass, file("persistanceClass.pck", "w"))                   
                
        if fname != "":                
            try:
                if fname.split('.')[-1].toLower() == 'edf' or \
                   fname.split('.')[-1].toLower() == 'bdf' :
                    self.reader = EDFReader(str(fname))
                elif fname.split('.')[-1].toLower() == 'sig' :
                    self.reader = HarmonieReader(str(fname))                
                else:
                    errMessage = QErrorMessage(self)   
                    errMessage.showMessage(QString("Error: " + "Type de fichier non supporté."))
                    return

                self.fileName = fname
                self.statusBar().showMessage('File: ' + fname)
                self.showSetSignalDlg()
                #self.reader.setPageDuration(self.pageDuration)
                self.pageDuration = self.reader.getPageDuration()

                print self.reader
                print self.reader.events
                self.pageTimeStart, self.pageDatetimeStart = self.reader.getPages()
                print self.pageTimeStart, self.pageDatetimeStart 
                self.currentPage = 1     
                self.pageOffset  = 0 #-self.pageTimeStart[0]

                self.loadFilters()

                self.setDisplayedSignals()       
                self.pageSlider.setRange(self.pageTimeStart[0], self.pageTimeStart[-1]+self.pageDuration)
                self.pageSlider.setValue(1)

                groupNames = np.unique([e.groupName for e in self.reader.events])
                typeNames  = np.unique([e.name for e in self.reader.events])
                self.displayEventGroups = dict.fromkeys(groupNames, True)
                self.displayEventTypes  = dict.fromkeys(typeNames, True)                 
                self.refreshDisplayEvents()
                
                self.updateSignalsPerPage()        
                self.updateEventWidgets()
                
            except IOError:  
                errMessage = QErrorMessage(self)            
                errMessage.showMessage(QString("Error: " + "The selected file could not be open."))
                return
                
    def refreshDisplayEvents(self):
        try:
            self.displayedEvents = [e for e in self.reader.events                  \
                                          if self.displayEventTypes[e.name] and  \
                                             self.displayEventGroups[e.groupName]\
                                 ]
        except KeyError:
            print "A group or an event type has been added to the reader without being"\
                  " added to the self.displayEventTypes and the self.displayEventGroups"\
                  " dictionnaries."
            raise
        
    

    def updateEventWidgets(self):
        self.refreshDisplayEvents()
                    
        self.eventGroupWidget.setTableData(self.displayEventGroups)
        self.eventTypeWidget.setTableData(self.displayEventTypes)      
        self.eventWidget.setEventData(self.displayedEvents, self.reader.getRecordingStartTime())


    """        
    def eventGroupUseChanged(self, name, display) :
        name = str(name)
        self.displayEventGroups[name] = display
        self.updateEventWidgets()        
        
    def eventTypeUseChanged(self, name, display) :
        name = str(name)
        self.displayEventTypes[name] = display             
        self.updateEventWidgets()    
    """        


    def eventGroupUseChangedAll(self, displayEventGroups) :

        self.displayEventGroups = displayEventGroups
        self.updateEventWidgets()        
        self.updateSignalsPerPage()
        
        
        
        
    def eventTypeUseChangedAll(self, displayEventTypes) :
        
        self.displayEventTypes = displayEventTypes             
        self.updateEventWidgets() 
        self.updateSignalsPerPage()           
        



    def loadFilters(self):
        if os.path.exists("FilterBankInfoClass.pck"):        
            self.filterBank = cPickle.load(file("FilterBankInfoClass.pck", "r"))               
        else:
            self.filterBank = []


    def setSignalBtnClicked(self):
        self.showSetSignalDlg()
        self.setDisplayedSignals()  
        self.updateSignalsPerPage()                        



    def showSetSignalDlg(self):
        if self.reader is None:
            errMessage = QErrorMessage(self)            
            errMessage.showMessage(QString("Error: " + "You first need to open a data file."))
        else:
            dlg = setSignalsDlg(self)
            dlg.setReader(self.reader)
            dlg.exec_()


############################################################################
# TODO: Réimplémenter comme un QWidget comprennant le plotteur, la barre de stage,
#       l'éiquette des électrodes et l'axe du temps avec la date et l'heure correcte
#
#

class PolysomPlotter(QWidget):
    def __init__(self, parent=None): 
        QWidget.__init__(self, parent)        
        
        #self.stageBand    = StageBand(self)
        self.plottingArea = MultiCanalDataPlot(self)
        self.labelLayout  = QVBoxLayout()

        self.layout = QGridLayout()
        #self.layout.addWidget(self.stageBand, 1, 2) 
        self.layout.addWidget(self.plottingArea, 1, 2) 
        self.layout.addLayout(self.labelLayout, 1, 1) 
        self.layout.setSpacing(0)
        self.layout.setMargin(0)

        #self.setSizePolicy(QSizePolicy.Expanding, QSizePolicy.Expanding)

        self.setLayout(self.layout)


    # Change la sélection de signaux à afficher...
    def setSignals(self, signalInfo):
        
        for i in range(self.labelLayout.count()): 
            self.labelLayout.itemAt(i).widget().close()
        for record in signalInfo.records :            
            self.labelLayout.addWidget(QLabel(record.name), record.space)         
                        
        self.plottingArea.setSignals(signalInfo)
        

        
        #TODO: Implémenter
        #self.plottingArea.showEvents(events)        

        
    # Met à jpurs les signaux, p. ex. suite à un déplacement sur l'axe temporelle
    def updateSignals(self, signalInfo, signalsData, reader, displayEvents, start, stop):        
       
        self.startPlotting = start
        self.stopPlotting  = stop

        # Liste des évènements de cette page
        events = reader.getEvents(self.startPlotting, self.stopPlotting)
        stageEvent = filter(lambda e: e.groupName.lower() == "stage", events)      
        
        if len(displayEvents):
            notStageEvent = filter(lambda e: e.groupName.lower() != "stage", events)      
            notStageDisplayEvent = np.array(displayEvents)[np.in1d(displayEvents, notStageEvent)]
        else:
            notStageDisplayEvent = []

        #self.stageBand.update(stageEvent, self.startPlotting, self.stopPlotting)

        self.plottingArea.drawEvents(notStageDisplayEvent, self.startPlotting, self.stopPlotting)     
        self.plottingArea.updateSignals(signalInfo, signalsData, reader, self.startPlotting, self.stopPlotting)        
        self.plottingArea.setPageFrontiers(stageEvent, self.startPlotting, self.stopPlotting)                            
        self.plottingArea.replot()       
        

        
        #TODO: Implémenter
        #self.plottingArea.showEvents(events)      





def main(args): 
    app = QApplication(args)
    win = MainWindow()
    win.show()  
    sys.exit(app.exec_())


def startNavigator():
    main([])

#if __name__ == '__main__':
#    arg = sys.argv
#    main(arg)

import cProfile
if __name__ == '__main__':   
    #cProfile.run('main(sys.argv)', 'profileReport')
    main(sys.argv)




# Local Variables: ***
# mode: python ***
# End: ***

