# -*- coding: utf-8 -*-
"""
Created on Wed Jul 11 12:38:40 2012

@author: REVESTECH
"""


#import  os, cPickle, copy

from PyQt4.QtCore import *
from PyQt4.QtGui import *
from PyQt4.QtWebKit import *
from PyQt4.QtSvg import *

from scipy import array, hamming, bartlett, blackman, hanning, fft, linspace,reshape, deg2rad
from scipy import arange, transpose, where, concatenate, zeros, sin, cos, optimize, shape
from scipy.integrate import cumtrapz, trapz
from scipy.fftpack import fftfreq
import numpy as np


import matplotlib.gridspec as gridspec
#from matplotlib import colorbar
from matplotlib.figure import Figure
from matplotlib.backends.backend_qt4agg import FigureCanvasQTAgg as FigureCanvas
from matplotlib.colors import Normalize, Colormap
from matplotlib import cm, pyplot

from math import pi

#from rpy_options import set_options
#set_options(RHOME='C:\\Program Files\\R\\R-2.12.1\\')
#from rpy import r


import time
import datetime


from spyndle import computeFastST_real,  computeMST, computeST
from spyndle import Filter


# http://stackoverflow.com/questions/2459295/stft-and-istft-in-python
def stft(x, fs, framesz, hop, t, window="hamming"):
    framesamp = int(framesz*fs)
    hopsamp = int(hop*fs)    
    w = eval( window + "(framesamp)")
    X = array([fft(w*x[i:i+framesamp])[0:(framesamp+1)/2]  for i in range(0, len(x)-framesamp, hopsamp)])
    tout = array([t[(2*i+framesamp)/2] for i in range(0, len(x)-framesamp, hopsamp)])
    fout = fftfreq(framesamp, 1.0/fs)[0:(framesamp+1)/2]
    return X, tout, fout
    
    
# http://lmf-ramblings.blogspot.ca/2009/07/multivariate-normal-distribution-in.html
#def multinormalPDF(b, mean, cov):    
#    #b: A vector
#    #mean: The mean of the elements in b (same dimensions as b)
#    #cov: The covariance matrix of the multivariate normal distribution
#    
#    k = shape(b)[0]
#    part1 = np.exp(-0.5*k*np.log(2*np.pi))
#    part2 = np.power(np.linalg.det(cov),-0.5)
#    dev = b-mean
#    part3 = np.exp(-0.5*np.dot(np.dot(dev.transpose(),np.linalg.inv(cov)),dev))
#    return part1*part2*part3     
#        
#




class showSpectogramToolDlg(QDialog):
    def __init__(self, parent): #, currentTime):
        QDialog.__init__(self, parent)

        self.setWindowTitle('Spectogram plotting tools')
        self.mainLayout = QGridLayout()        
        self.setLayout(self.mainLayout)  
        self.MainWin = parent
        self.resize(700,600)


        self.zmax = None

        ######################################################################
        # Key shortcut events
        self.returnKeySC = QShortcut(QKeySequence(Qt.Key_Return), self)
        self.returnKeySC.activated.connect(self.enterKeyPressed)

        self.upKeySC = QShortcut(QKeySequence(Qt.Key_Up), self)
        self.upKeySC.activated.connect(self.upKeyPressed)
        
        self.downKeySC = QShortcut(QKeySequence(Qt.Key_Down), self)
        self.downKeySC.activated.connect(self.downKeyPressed)
        
        self.leftKeySC = QShortcut(QKeySequence(Qt.Key_Left), self)
        self.leftKeySC.activated.connect(self.leftKeyPressed)
        
        self.rightKeySC = QShortcut(QKeySequence(Qt.Key_Right), self)
        self.rightKeySC.activated.connect(self.rightKeyPressed)





        
        ####################################################################
        # Single graph
        gs = gridspec.GridSpec(2, 3,width_ratios=[2,8,1], height_ratios=[4,1])
        gs.update(left=0.075, right=0.95, wspace=0.025, hspace=0.025, top=0.95, bottom=0.075)

        self.figSingle = Figure(figsize=(600,600), dpi=72, facecolor=(1,1,1), edgecolor=(0,0,0))

        self.axContour  = self.figSingle.add_subplot(gs[1])
        self.axTime     = self.figSingle.add_subplot(gs[4])
        self.axColorbar = self.figSingle.add_subplot(gs[2])
        self.axFreq     = self.figSingle.add_subplot(gs[0])
      
        self.axContour.get_xaxis().set_visible(False)
        self.axContour.get_yaxis().set_visible(False)

        self.axFreq.get_xaxis().set_visible(False)
        self.axTime.get_yaxis().set_visible(False)
        
        self.axFreq.get_yaxis().set_label_text("Frequency (Hz)")
        self.axTime.get_xaxis().set_label_text("time (s)")

        self.spectogramPlot = FigureCanvas(self.figSingle) #contourPlot() # Qwt.QwtPlot(self)            
        

        ####################################################################
        # Multiple graphs
        gs_multiple = gridspec.GridSpec(3, 2,width_ratios=[1,1], height_ratios=[1,1, 1])
        gs_multiple.update(left=0.075, right=0.95, wspace=0.025, hspace=0.025, top=0.95, bottom=0.075)

        fig_multiple = Figure(figsize=(600,600), dpi=72, facecolor=(1,1,1), edgecolor=(0,0,0))

        self.axMultiple = []
        for i in range(6):
            self.axMultiple.append(fig_multiple.add_subplot(gs_multiple[i]))
      
            self.axMultiple[i].get_xaxis().set_visible(False)
            self.axMultiple[i].get_yaxis().set_visible(False)

            #self.axMultiple[i].get_yaxis().set_label_text("Frequency (Hz)")
            #self.axMultiple[i].get_xaxis().set_label_text("time (s)")

        self.multipleAmplitudePlot = FigureCanvas(fig_multiple) #contourPlot() # Qwt.QwtPlot(self)            
        
        
        ####################################################################
        # In/Out of phase graphs
        gs_inOutPhase = gridspec.GridSpec(3, 4,width_ratios=[1,1,1,1], height_ratios=[1,1,1])
        gs_inOutPhase.update(left=0.075, right=0.95, wspace=0.025, hspace=0.025, top=0.95, bottom=0.075)

        fig_inOutPhase = Figure(figsize=(600,600), dpi=72, facecolor=(1,1,1), edgecolor=(0,0,0))

        self.axInOutPhase = []
        for i in range(12):
            self.axInOutPhase.append(fig_inOutPhase.add_subplot(gs_inOutPhase[i]))
      
            self.axInOutPhase[i].get_xaxis().set_visible(False)
            self.axInOutPhase[i].get_yaxis().set_visible(False)

            #self.axMultiple[i].get_yaxis().set_label_text("Frequency (Hz)")
            #self.axMultiple[i].get_xaxis().set_label_text("time (s)")

        self.inOutPhasePlot = FigureCanvas(fig_inOutPhase) #contourPlot() # Qwt.QwtPlot(self)            
        
        


        #################################################################
        # Computation tab
        self.pageComputation = QWidget()
          
        self.STFTRadio      = QRadioButton("Short time Fourier Transform", self.pageComputation)
        self.frameSizeWgt   = QLineEdit("0.5")  
        self.hopSizeWgt     = QLineEdit("0.01")       
        self.windowCombo    = QComboBox()         
        #for item in ["hamming", "bartlett", "blackman", "hanning"]:
        #    self.windowCombo.addItem
        self.windowCombo.addItems(["hamming", "bartlett", "blackman", "hanning"])
        self.windowCombo.setCurrentIndex(0)
       
        
        
        self.STRadio        = QRadioButton("S Transform",  self.pageComputation)
        self.FSTRadio        = QRadioButton("Fast S Transform",  self.pageComputation)
        
        self.MSTRadio       = QRadioButton("Modified S Transform",  self.pageComputation)
        self.MSFParammWgt   = QLineEdit("0.0")  
        self.MSFParamkWgt   = QLineEdit("1.0")         

        
        self.STRadio.setChecked(True)        
        
        #self.STFTRadio.toggled.connect(self.STFTRadioChanged)
        #self.STRadio.toggled.connect(self.STRadioChanged)
        
    
        self.computationLayout = QGridLayout()           
        
        self.computationLayout.addWidget(self.STFTRadio, 1, 1)
        self.computationLayout.addWidget(QLabel("Frame size: "), 1, 2)
        self.computationLayout.addWidget(self.frameSizeWgt, 1, 3)
        self.computationLayout.addWidget(QLabel("Hop size: "), 1, 4)
        self.computationLayout.addWidget(self.hopSizeWgt, 1, 5)
        self.computationLayout.addWidget(QLabel("Window: "), 1, 6)
        self.computationLayout.addWidget(self.windowCombo, 1, 7)

  
        self.computationLayout.addWidget(self.STRadio, 2, 1)
        self.computationLayout.addWidget(self.FSTRadio, 3, 1)
        
        self.computationLayout.addWidget(self.MSTRadio, 4, 1)
        self.computationLayout.addWidget(QLabel("m: "), 4, 2)
        self.computationLayout.addWidget(self.MSFParammWgt, 4, 3)
        self.computationLayout.addWidget(QLabel("k: "), 4, 4)
        self.computationLayout.addWidget(self.MSFParamkWgt, 4, 5)
   
        self.pageComputation.setLayout(self.computationLayout)






        ####################################################################
        # Localisation tab
        self.pageLocalisation = QWidget()        
        
        # Donne le sample correspondant approximativement à currentTime 
        self.currentSampleStart = self.MainWin.pageSampleStart[self.MainWin.currentPage-1] + self.MainWin.pageOffset 
        self.currentDatetime = ( self.MainWin.pageDatetimeStart[self.MainWin.currentPage-1] +
                            datetime.timedelta(seconds = self.MainWin.pageOffset/self.MainWin.reader.baseFreq))
        self.timeStartWgt   = QTimeEdit(self.currentDatetime.time())  
        self.durationWgt    = QLineEdit("4.0")  
        
        self.currentTimeRadio   = QRadioButton("current time", self.pageLocalisation)
        self.eventTimeRadio     = QRadioButton("event based",  self.pageLocalisation)
        
        self.currentTimeRadio.toggled.connect(self.currentTimeRadioChanged)
        self.eventTimeRadio.toggled.connect(self.eventTimeRadioChanged)


            
        self.timeStartWgt.setDisplayFormat("hh:mm:ss.zzz")
        self.timeStartWgt.setEnabled(False)

        self.channelCombo = QComboBox()     
        self.montageCombo = QComboBox()     

        self.montageCombo.currentIndexChanged.connect(self.montageChanged)    

        self.montageCombo.addItem(self.MainWin.reader.IRecordingMontage.GetMontageName())        
        for i in range(self.MainWin.reader.IRecordingMontage.GetReformattingMontageCount()):
            self.montageCombo.addItem(self.MainWin.reader.IRecordingMontage.GetReformattingMontage(i).GetMontageName())
        
        self.montageCombo.setCurrentIndex(0)                   
  
        self.box = QGridLayout()        
        

        self.box.addWidget(QLabel("Montage: "), 1, 1)
        self.box.addWidget(self.montageCombo, 1, 2)
        self.box.addWidget(QLabel("Channel: "), 2, 1)
        self.box.addWidget(self.channelCombo, 2, 2)        


        self.box.addWidget(self.currentTimeRadio, 1, 3)      
        self.box.addWidget(self.eventTimeRadio, 2, 3)            
                
        
        self.box.addWidget(QLabel("Time start: "), 1, 4)
        self.box.addWidget(self.timeStartWgt, 1, 5)
        self.box.addWidget(QLabel("Duration: "), 1, 6)
        self.box.addWidget(self.durationWgt, 1, 7)
        
        
        
        self.eventTypeWdt   = QComboBox()
        for name in list(set(map(lambda e: e.name, self.MainWin.reader.events))):
            self.eventTypeWdt.addItem(name)
                
        self.eventTypeWdt.currentIndexChanged.connect(self.eventTypeChanged)            
         

        self.beforeEventWdt = QLineEdit("0.5")
        self.afterEventWdt  = QLineEdit("0.5")
        
        self.eventWdt       = QComboBox()    
        self.eventWdt.currentIndexChanged.connect(self.eventChanged)  
        self.eventWdt.setFixedWidth(100)
            
        self.computeNormalChk = QCheckBox("Compute normal")        
        self.computeRegChk    = QCheckBox("Compute regression")        
        
        self.box.addWidget(QLabel("Event type: "), 2, 4)
        self.box.addWidget(self.eventTypeWdt, 2, 5)
        self.box.addWidget(QLabel("Before: "), 2, 6)
        self.box.addWidget(self.beforeEventWdt, 2, 7)
        self.box.addWidget(QLabel("After: "), 2, 8)
        self.box.addWidget(self.afterEventWdt, 2, 9)
        self.box.addWidget(QLabel("Event: "), 2, 10)
        self.box.addWidget(self.eventWdt, 2, 11)
        self.box.addWidget(self.computeNormalChk, 2, 12)
        self.box.addWidget(self.computeRegChk, 2, 13)
                
        self.computeNormalChk.setChecked(False)
        self.computeRegChk.setChecked(False)
        
        self.currentTimeRadio.setChecked(True)
        self.eventTimeRadioChanged()        

        self.pageLocalisation.setLayout(self.box)
        
        
        
        
    
        
        
        ######################################################################
        # Graphs tab       
        self.timeMinWgt = QLineEdit()
        self.timeMaxWgt = QLineEdit()
        self.freqMinWgt = QLineEdit()
        self.freqMaxWgt = QLineEdit()   
        self.ZMaxWgt    = QLineEdit()   
        self.automaticTChkBox = QCheckBox()     
        self.automaticFChkBox = QCheckBox()       
        self.automaticZChkBox = QCheckBox()           
        
        self.boxGraphsProp = QGridLayout()        

        self.boxGraphsProp.addWidget(QLabel("Time min.: "), 1, 1)
        self.boxGraphsProp.addWidget(self.timeMinWgt, 1, 2)
        self.boxGraphsProp.addWidget(QLabel("Time max.: "), 1, 3)
        self.boxGraphsProp.addWidget(self.timeMaxWgt, 1, 4)
        self.boxGraphsProp.addWidget(QLabel("Automatic: "), 1, 5)  
        self.boxGraphsProp.addWidget(self.automaticTChkBox, 1, 6)   
        
        self.boxGraphsProp.addWidget(QLabel("Freq. min: "), 2, 1)
        self.boxGraphsProp.addWidget(self.freqMinWgt, 2, 2)
        self.boxGraphsProp.addWidget(QLabel("Freq. max: "), 2, 3)  
        self.boxGraphsProp.addWidget(self.freqMaxWgt, 2, 4)      
        self.boxGraphsProp.addWidget(QLabel("Automatic: "), 2, 5)  
        self.boxGraphsProp.addWidget(self.automaticFChkBox, 2, 6)  
        
        self.boxGraphsProp.addWidget(QLabel("Z. max: "), 3, 3)  
        self.boxGraphsProp.addWidget(self.ZMaxWgt, 3, 4)      
        self.boxGraphsProp.addWidget(QLabel("Automatic: "), 3, 5)  
        self.boxGraphsProp.addWidget(self.automaticZChkBox, 3, 6)  
        
        self.automaticTChkBox.stateChanged.connect(self.automatiTCheckBoxChanged)
        self.automaticTChkBox.setChecked(True)
        
        self.automaticFChkBox.stateChanged.connect(self.automatiFCheckBoxChanged)
        self.automaticFChkBox.setChecked(True)
        
        self.automaticZChkBox.stateChanged.connect(self.automatiZCheckBoxChanged)
        self.automaticZChkBox.setChecked(True)
        
        self.pageGraphs = QWidget()
        self.pageGraphs.setLayout(self.boxGraphsProp)
        
        
        
        
        
        
        

        ######################################################################
        # Filtering tab       
        self.lowCutWgt      = QLineEdit("10.0")
        self.highCutWgt     = QLineEdit("16.0")
        self.lowCutFiltChk  = QCheckBox()
        self.highCutFiltChk  = QCheckBox()

        self.lowCutFiltChk.setChecked(False)
        self.highCutFiltChk.setChecked(False)
        
        self.boxFilteringProp = QGridLayout()        

        self.boxFilteringProp.addWidget(QLabel("Low cutting frequency:"), 1, 1)
        self.boxFilteringProp.addWidget(self.lowCutWgt, 1, 2)
        self.boxFilteringProp.addWidget(self.lowCutFiltChk, 1, 3)   
        
        self.boxFilteringProp.addWidget(QLabel("High cutting frequency:"), 2, 1)
        self.boxFilteringProp.addWidget(self.highCutWgt, 2, 2)
        self.boxFilteringProp.addWidget(self.highCutFiltChk, 2, 3)   

        
        self.pageFiltring = QWidget()
        self.pageFiltring.setLayout(self.boxFilteringProp)
        
        

        
        
        
        #######################################################################
        # Tab widget proprietes
        self.tabs = QTabWidget()
        self.tabs.addTab(self.pageLocalisation, "Localisation")    
        self.tabs.addTab(self.pageComputation,  "Computation")    
        self.tabs.addTab(self.pageGraphs,       "Graphs")     
        self.tabs.addTab(self.pageFiltring,     "Filtering")    
        




        #######################################################################
        self.tabsAffichage = QTabWidget()
        self.tabsAffichage.addTab(self.spectogramPlot, "Single amplitude")    
        self.tabsAffichage.addTab(self.multipleAmplitudePlot,  "Multiple amplitude")    
        self.tabsAffichage.addTab(self.inOutPhasePlot,  "In/Out of phase")    



        ####################################################################
        # Main                 
        
        self.plotBtn        = QPushButton("Plot")
        self.saveBtn        = QPushButton("Save")
        self.plotBtn.clicked.connect(self.enterKeyPressed)           
        self.saveBtn.clicked.connect(self.savePlot)           
      
        self.btnGroupeLayout = QVBoxLayout()
     
        self.btnGroupeLayout.addWidget(self.plotBtn)
        self.btnGroupeLayout.addWidget(self.saveBtn)     
     
        self.btnGroupe      = QWidget()        
        self.btnGroupe.setLayout(self.btnGroupeLayout)
      
        self.mainLayout.addWidget(self.tabs, 1, 1, 2, 1)           
        self.mainLayout.addWidget(self.btnGroupe, 1, 2)                            
        self.mainLayout.addWidget(self.tabsAffichage, 3, 1, 1, 2)   


    def updateAffichage(self):
        
        if(self.tabsAffichage.currentIndex() == 0):        
            self.plotSpectogram()             
        elif(self.tabsAffichage.currentIndex() == 1):
            self.plotMultiple()  
        elif(self.tabsAffichage.currentIndex() == 2):
            self.plotInOutPhase()  
                    

    def savePlot(self):
        
        if(self.tabsAffichage.currentIndex() == 0):        
            widget = self.spectogramPlot          
        elif(self.tabsAffichage.currentIndex() == 1):
            widget = self.multipleAmplitudePlot
        elif(self.tabsAffichage.currentIndex() == 2):
            widget = self.inOutPhasePlot    
        
        fileName =  QFileDialog.getSaveFileName(self, "Save plot");
              
        if not fileName.isEmpty():
            widget.print_svg(fileName + ".svg")
            widget.print_png(fileName + ".png")



    def eventTypeChanged(self):
        self.eventWdt.clear()
        for event in filter(lambda e: e.name == str(self.eventTypeWdt.currentText()) and e.channel == str(self.channelCombo.currentText()), self.MainWin.reader.events):
            self.eventWdt.addItem(event.dateTime.time().strftime("%H:%M:%S.%f")[:-3])

    def eventChanged(self):
        events = filter(lambda e: e.name == str(self.eventTypeWdt.currentText())  and e.channel == str(self.channelCombo.currentText()), self.MainWin.reader.events)
        self.currentSampleStart = events[self.eventWdt.currentIndex()].startSample -  int(float(self.beforeEventWdt.text())*self.MainWin.reader.baseFreq)      
        self.duration = events[self.eventWdt.currentIndex()].timeLength + float(self.beforeEventWdt.text()) + float(self.afterEventWdt.text())
     
        self.updateAffichage()             

            
            
    def currentTimeRadioChanged(self):
        self.durationWgt.setEnabled(self.currentTimeRadio.isChecked())

    def eventTimeRadioChanged(self):
        self.eventTypeWdt.setEnabled(self.eventTimeRadio.isChecked())
        self.beforeEventWdt.setEnabled(self.eventTimeRadio.isChecked())
        self.afterEventWdt.setEnabled(self.eventTimeRadio.isChecked())
        self.eventWdt.setEnabled(self.eventTimeRadio.isChecked())       
               
        
        
        
    def upKeyPressed(self):
        if self.currentTimeRadio.isChecked() :        
            duration = float(self.durationWgt.text())
            self.currentSampleStart -= int(duration*0.95*self.MainWin.reader.baseFreq) 
            
            self.updateAffichage()  

        elif self.eventTimeRadio.isChecked() :           
            self.eventWdt.setCurrentIndex(max(0, self.eventWdt.currentIndex()-1))     
            # Le reste du traitement est faite automatiquement par eventChanged
            #events = filter(lambda e: e.name == str(self.eventTypeWdt.currentText())  and e.channel == str(self.channelCombo.currentText()), self.MainWin.reader.events)
            #self.currentSampleStart = events[self.eventWdt.currentIndex()].startSample -  int(float(self.beforeEventWdt.text())*self.MainWin.reader.baseFreq)      
            #self.duration = events[self.eventWdt.currentIndex()].timeLength + float(self.beforeEventWdt.text()) + float(self.afterEventWdt.text())
    
        
        

    def downKeyPressed(self):   
        if self.currentTimeRadio.isChecked() :   
            duration = float(self.durationWgt.text())
            self.currentSampleStart += int(duration*0.95*self.MainWin.reader.baseFreq)    

            self.updateAffichage() 

        elif self.eventTimeRadio.isChecked() :           
            self.eventWdt.setCurrentIndex(min(self.eventWdt.count()-1, self.eventWdt.currentIndex()+1))   
            # Le reste du traitement est faite automatiquement par eventChanged
            #events = filter(lambda e: e.name == str(self.eventTypeWdt.currentText())  and e.channel == str(self.channelCombo.currentText()), self.MainWin.reader.events)
            #self.currentSampleStart = events[self.eventWdt.currentIndex()].startSample -  int(float(self.beforeEventWdt.text())*self.MainWin.reader.baseFreq)               
            #self.duration = events[self.eventWdt.currentIndex()].timeLength + float(self.beforeEventWdt.text()) + float(self.afterEventWdt.text()) 
            
         


    def rightKeyPressed(self):
        #if self.currentTimeRadio.isChecked() :  
        if self.currentTimeRadio.isChecked() :   
            self.duration = float(self.durationWgt.text())   
        elif self.eventTimeRadio.isChecked() :           
            events = filter(lambda e: e.name == str(self.eventTypeWdt.currentText())  and e.channel == str(self.channelCombo.currentText()), self.MainWin.reader.events)
            self.duration = events[self.eventWdt.currentIndex()].timeLength + float(self.beforeEventWdt.text()) + float(self.afterEventWdt.text())
            
        self.currentSampleStart += int(self.duration*0.05*self.MainWin.reader.baseFreq)    

        self.updateAffichage() 
            
        
        

    def leftKeyPressed(self):
        #if self.currentTimeRadio.isChecked() :   
        if self.currentTimeRadio.isChecked() :   
            self.duration = float(self.durationWgt.text())   
        elif self.eventTimeRadio.isChecked() :           
            events = filter(lambda e: e.name == str(self.eventTypeWdt.currentText())  and e.channel == str(self.channelCombo.currentText()), self.MainWin.reader.events)
            self.duration = events[self.eventWdt.currentIndex()].timeLength + float(self.beforeEventWdt.text()) + float(self.afterEventWdt.text())
                        
        self.currentSampleStart -= int(self.duration*0.05*self.MainWin.reader.baseFreq)    

        self.updateAffichage() 
            

                
    def enterKeyPressed(self):        
        if self.currentTimeRadio.isChecked() :   
            self.duration = float(self.durationWgt.text())   
        elif self.eventTimeRadio.isChecked() :           
            events = filter(lambda e: e.name == str(self.eventTypeWdt.currentText())  and e.channel == str(self.channelCombo.currentText()), self.MainWin.reader.events)
            self.duration = events[self.eventWdt.currentIndex()].timeLength + float(self.beforeEventWdt.text()) + float(self.afterEventWdt.text())
      
        self.updateAffichage()  
            



 

    def automatiTCheckBoxChanged(self, state):

        self.timeMinWgt.setEnabled(not state)#self.automaticChkBox.isChecked())
        self.timeMaxWgt.setEnabled(not state)
       
        tmin, tmax = self.axTime.get_xlim()     

        if not state:
            self.timeMinWgt.setText(str(tmin))
            self.timeMaxWgt.setText(str(tmax))
            
            
    def automatiFCheckBoxChanged(self, state):

        self.freqMinWgt.setEnabled(not state)
        self.freqMaxWgt.setEnabled(not state)
       
        fmin, fmax = self.axFreq.get_ylim()       
       
        if not state:
            self.freqMinWgt.setText(str(fmin))
            self.freqMaxWgt.setText(str(fmax)) 
            
            
    def automatiZCheckBoxChanged(self, state):

        self.ZMaxWgt.setEnabled(not state)

        if not state:
            if self.zmax is None:
                self.zmax = 1.0            
            self.ZMaxWgt.setText(str(self.zmax))
            
            
       
#
#    def optimMultiNormFit(self, param):
#
#        start_time  = time.time()
#        mapShape    = shape(self.Xmap)
#        #error       = zeros(mapShape)        
#        N           = zeros(mapShape) 
#        #xMax = max(max(self.Xmap))        
#        
#        cov = reshape(param[3:], (2, 2))
#        mean  = param[1:3]
#        
#        # multinormal map
#        k = shape(mean)[0]
#        part1 = np.exp(-0.5*k*np.log(2*np.pi))
#        part2 = np.power(np.linalg.det(cov),-0.5)        
#        for i0 in range(mapShape[0]):
#            for i1 in range(mapShape[1]):  
#                dev = [i0, i1]-mean
#                part3 = np.exp(-0.5*np.dot(np.dot(dev.transpose(),np.linalg.inv(cov)),dev))
#                N[i0, i1] = part1*part2*part3             
#
#        return (self.Xmap - N).ravel()
#     
            
    def plotMultiple(self):            

        start =  self.currentSampleStart 
        
        channelList = ['f3-ref', 'f4-ref', 'c3-ref', 'c4-ref', 'o1-ref', 'o2-ref']        

        signalsData = self.MainWin.reader.read(channelList, start, self.duration)  
    
        framesz  = float(self.frameSizeWgt.text())          # with a frame size of 50 milliseconds
        hop      = float(self.hopSizeWgt.text())            # and hop size of 20 milliseconds.
            
        for i in range(6) :
        
            self.axMultiple[i].set_title(channelList[i])
            
            signal      = signalsData[1][i]   
            fs          = signalsData[0][i]                       # sampling rate   
            chanType    = signalsData[2][i]       
            
            if self.lowCutFiltChk.isChecked():           
                highPassFilter = Filter(fs)
                highPassFilter.create(chanType, low_crit_freq=float(self.lowCutWgt.text()), 
                                      high_crit_freq=None, order=4, btype="highpass", ftype="butter", useFiltFilt=True)                 
                signal = highPassFilter.applyFilter(signal)        
            
            if self.highCutFiltChk.isChecked():
                lowPassFilter = Filter(fs)
                lowPassFilter.create(chanType, low_crit_freq=None, 
                                      high_crit_freq=float(self.highCutWgt.text()), order=4, btype="lowpass", ftype="butter", useFiltFilt=True)                
                signal = lowPassFilter.applyFilter(signal)                
            
            
            self.timeStartWgt.setTime(signalsData[3].time())  
            
            t  = linspace(0, self.duration, self.duration*fs, endpoint=False)
            tX = t
            
            if hop*fs < 1.0:
                errorMsg = QErrorMessage(self)
                errorMsg.showMessage("Hop size must be larger or equat to " + str(1.0/fs) +  " for a sampling frequency of " + str(fs) + "Hz.")            
                self.hopSizeWgt.setText(str(1.0/fs))
                hop = 1.0/fs 
    
            # Create STFT.
            if self.STFTRadio.isChecked() :
                X, tX, fX = stft(signal, fs, framesz, hop, t, str(self.windowCombo.currentText()))
            elif self.STRadio.isChecked() :
                X, fX = computeST(signal, fs)    
            elif self.FSTRadio.isChecked() :
                X, fX = computeFastST_real(signal, fs)    
            elif self.MSTRadio.isChecked() :
                X, fX = computeMST(signal, fs, float(self.MSFParammWgt.text()), float(self.MSFParamkWgt.text()))  
                    
    
    
            if not self.automaticTChkBox.isChecked():
                tmin = float(self.timeMinWgt.text())
                tmax = float(self.timeMaxWgt.text())   
            else:
                tmin = min(tX)
                tmax = max(tX)
    
            if not self.automaticFChkBox.isChecked():
                fmin = float(self.freqMinWgt.text())
                fmax = float(self.freqMaxWgt.text())       
            else:
                fmin = min(fX)
                fmax = max(fX)
    
            if not self.automaticZChkBox.isChecked():
                self.zmax = float(self.ZMaxWgt.text())  
            else:
                self.zmax = np.max(np.max(abs(X)))            
                


            self.axMultiple[i].cla()

            self.axMultiple[i].set_xlim(tmin, tmax)
            self.axMultiple[i].set_ylim(fmin, fmax)

            

            norm = Normalize(vmin=0, vmax=self.zmax)
    
            indT = where((array(tX) >= tmin)*(array(tX) <= tmax))[0]
            indF = where((array(fX) >= fmin)*(array(fX) <= fmax))[0]
            self.axMultiple[i].imshow(abs(transpose(X[indT[0]:indT[-1], indF[0]:indF[-1]])), aspect='auto', 
                                    extent=[tX[indT[0]], tX[indT[-1]], fX[indF[0]], fX[indF[-1]]], 
                                        cmap=cm.get_cmap("jet"), norm=norm, origin="lower") # 
    
        self.multipleAmplitudePlot.draw()        
        





        
            
    def plotInOutPhase(self):

        start =  self.currentSampleStart 
        
        refChannel = 'c3-ref'       
        
        channelList = ['f3-ref', 'f4-ref', 'c3-ref', 'c4-ref', 'o1-ref', 'o2-ref']        

        signalsDataCmp = self.MainWin.reader.read(channelList, start, self.duration)  
        
        signalsDataRef = self.MainWin.reader.read([refChannel], start, self.duration)  
    
        framesz  = float(self.frameSizeWgt.text())          # with a frame size of 50 milliseconds
        hop      = float(self.hopSizeWgt.text())            # and hop size of 20 milliseconds.
            
        self.X  = []
        self.tX = []
        self.fX = []            
            
            
        # Calcul des spectres
        for i in range(7) :
            if i == 0 :
                signal      = signalsDataRef[1][0]   
                fs          = signalsDataRef[0][0]                       # sampling rate   
                chanType    = signalsDataRef[2][0]        
                signalRecord= signalsDataRef[3]    
            else:
                signal      = signalsDataCmp[1][i-1]   
                fs          = signalsDataCmp[0][i-1]                       # sampling rate   
                chanType    = signalsDataCmp[2][i-1]    
                signalRecord= signalsDataCmp[3]                  
            
            if self.lowCutFiltChk.isChecked():           
                highPassFilter = Filter(fs)
                highPassFilter.create(chanType, low_crit_freq=float(self.lowCutWgt.text()), 
                                      high_crit_freq=None, order=4, btype="highpass", ftype="butter", useFiltFilt=True)                 
                signal = highPassFilter.applyFilter(signal)        
            
            if self.highCutFiltChk.isChecked():
                lowPassFilter = Filter(fs)
                lowPassFilter.create(chanType, low_crit_freq=None, 
                                      high_crit_freq=float(self.highCutWgt.text()), order=4, btype="lowpass", ftype="butter", useFiltFilt=True)                
                signal = lowPassFilter.applyFilter(signal)                
            
            
            self.timeStartWgt.setTime(signalRecord.time())  
            
            t        = linspace(0, self.duration, self.duration*fs, endpoint=False)
    
            
            if hop*fs < 1.0:
                errorMsg = QErrorMessage(self)
                errorMsg.showMessage("Hop size must be larger or equat to " + str(1.0/fs) +  " for a sampling frequency of " + str(fs) + "Hz.")            
                self.hopSizeWgt.setText(str(1.0/fs))
                hop = 1.0/fs 
    
            # Create STFT.
            if self.STFTRadio.isChecked() :
                X, tX, fX = stft(signal, fs, framesz, hop, t, str(self.windowCombo.currentText()))
            elif self.STRadio.isChecked() :
                X, fX = computeST(signal, fs)    
                tX = t #linspace(0, self.duration, self.duration*fs*4.0, endpoint=False)     
            elif self.FSTRadio.isChecked() :
                X, fX = computeFastST_real(signal, fs)                   
                tX = t #linspace(0, self.duration, self.duration*fs*4.0, endpoint=False)                                    
            elif self.MSTRadio.isChecked() :
                X, fX = computeMST(signal, fs, float(self.MSFParammWgt.text()), float(self.MSFParamkWgt.text()))  
                tX = t #linspace(0, self.duration, self.duration*fs*4.0, endpoint=False)       
                    
            self.X.append(X)
            self.tX.append(tX)
            self.fX.append(fX)
    
    


    
        if not self.automaticTChkBox.isChecked():
            tmin = float(self.timeMinWgt.text())
            tmax = float(self.timeMaxWgt.text())   
        else:
            tmin = min(self.tX[0])
            tmax = max(self.tX[0])

        if not self.automaticFChkBox.isChecked():
            fmin = float(self.freqMinWgt.text())
            fmax = float(self.freqMaxWgt.text())       
        else:
            fmin = min(self.fX[0])
            fmax = max(self.fX[0])

        if not self.automaticZChkBox.isChecked():
            self.zmax = float(self.ZMaxWgt.text())  
        else:
            self.zmax = np.max(np.max(self.X[0]**2))       

        for i in range(6) :
        
            #self.axInOutPhase[i].set_title(channelList[i])    
            # In
            self.axInOutPhase[i*2].cla()
            self.axInOutPhase[i*2].set_xlim(tmin, tmax)
            self.axInOutPhase[i*2].set_ylim(fmin, fmax)
            
            # Out
            self.axInOutPhase[i*2+1].cla()
            self.axInOutPhase[i*2+1].set_xlim(tmin, tmax)
            self.axInOutPhase[i*2+1].set_ylim(fmin, fmax)

            

            norm = Normalize(vmin=0, vmax=self.zmax)
    
            indT = where((array(self.tX[0]) >= tmin)*(array(self.tX[0]) <= tmax))[0]
            indF = where((array(self.fX[0]) >= fmin)*(array(self.fX[0]) <= fmax))[0]
            
            
            X1 = self.X[0]            
            X2 = self.X[i+1]            
            
            X1 = transpose(X1[indT[0]:indT[-1], indF[0]:indF[-1]])   
            X2 = transpose(X2[indT[0]:indT[-1], indF[0]:indF[-1]])          
            
            Mat = X1*np.conjugate(X2)            
            
            self.axInOutPhase[i*2].imshow(np.real(Mat), aspect='auto', 
                                    extent=[tX[indT[0]], tX[indT[-1]], fX[indF[0]], fX[indF[-1]]], 
                                        cmap=cm.get_cmap("jet"), norm=norm, origin="lower") #             
            self.axInOutPhase[i*2+1].imshow(np.imag(Mat), aspect='auto', 
                                    extent=[tX[indT[0]], tX[indT[-1]], fX[indF[0]], fX[indF[-1]]], 
                                        cmap=cm.get_cmap("jet"), norm=norm, origin="lower") # 
        
            #self.axInOutPhase[i*2+1].imshow(np.angle(Mat), aspect='auto', 
             #                       extent=[tX[indT[0]], tX[indT[-1]], fX[indF[0]], fX[indF[-1]]], 
              #                          cmap=cm.get_cmap("jet"), origin="lower") # 
    

        
        self.inOutPhasePlot.draw()
        
        
        
    def plotSpectogram(self):

        start =  self.currentSampleStart 
        
        signalsData = self.MainWin.reader.read([str(self.channelCombo.currentText())], start, self.duration)          

        signal      = signalsData[1][0]   
        fs          = signalsData[0][0]                       # sampling rate   
        chanType    = signalsData[2][0]       
        
        
        #######################################################################
        # Set-up simulated data for testing.        
        #
        #fs      = 1024; # Hz        
        #t       = arange(self.duration*fs)/fs
        #print t, 
        #signal  = cos(t*2*pi*16.0) #+ cos(t/deg2rad(13)) + cos(t/deg2rad(16))
        #
        #######################################################################
        

        
        # TODO: À améliorer pour ne pas toujours créer ces filtres...
        if self.lowCutFiltChk.isChecked():
            highPassFilter = Filter(fs)
            highPassFilter.create(chanType, low_crit_freq=float(self.lowCutWgt.text()), 
                                  high_crit_freq=None, order=4, btype="highpass", ftype="butter", useFiltFilt=True)                
            signal = highPassFilter.applyFilter(signal)        
        
        if self.highCutFiltChk.isChecked():
            lowPassFilter = Filter(fs)
            lowPassFilter.create(chanType, low_crit_freq=None, 
                                  high_crit_freq=float(self.highCutWgt.text()), order=4, btype="lowpass", ftype="butter", useFiltFilt=True)                
            signal = lowPassFilter.applyFilter(signal)                
        
        
        
        
       
        self.timeStartWgt.setTime(signalsData[3].time())  

        t = linspace(0, self.duration, self.duration*fs, endpoint=False)
            
        # Create STFT.
        if self.STFTRadio.isChecked() :
            
            framesz  = float(self.frameSizeWgt.text())          # with a frame size of 50 milliseconds
            hop      = float(self.hopSizeWgt.text())            # and hop size of 20 milliseconds.
    
            
            if hop*fs < 1.0:
                errorMsg = QErrorMessage(self)
                errorMsg.showMessage("Hop size must be larger or equat to " + str(1.0/fs) +  " for a sampling frequency of " + str(fs) + "Hz.")            
                self.hopSizeWgt.setText(str(1.0/fs))
                hop = 1.0/fs 
            
            X, tX, fX = stft(signal, fs, framesz, hop, t, str(self.windowCombo.currentText()))
            
        # Create S transform
        elif self.STRadio.isChecked() :
            X, fX = computeST(signal, fs)    
            tX = t #linspace(0, self.duration, self.duration*fs*4.0, endpoint=False)     


        elif self.FSTRadio.isChecked() :
            X, fX = computeFastST_real(signal, fs)                   
            tX = t #linspace(0, self.duration, self.duration*fs*4.0, endpoint=False)       


        # Create modified S transform
        elif self.MSTRadio.isChecked() :
            X, fX = computeMST(signal, fs, float(self.MSFParammWgt.text()), float(self.MSFParamkWgt.text()))  
            tX = t #linspace(0, self.duration, self.duration*fs*4.0, endpoint=False)       
                
             
        
        
        self.axContour.cla()
        self.axTime.cla()
        self.axFreq.cla()


        f = fftfreq(signal.size, 1.0/fs)[0:((signal.size+1)/2)]
        
        if not self.automaticTChkBox.isChecked():
            tmin = float(self.timeMinWgt.text())
            tmax = float(self.timeMaxWgt.text())   
        else:
            tmin = min(tX)
            tmax = max(tX)

        if not self.automaticFChkBox.isChecked():
            fmin = float(self.freqMinWgt.text())
            fmax = float(self.freqMaxWgt.text())       
        else:
            fmin = min(fX)
            fmax = max(fX)

        if not self.automaticZChkBox.isChecked():
            self.zmax = float(self.ZMaxWgt.text())  
        else:
            self.zmax = np.max(np.max(abs(X)))
            
            
        self.axContour.set_xlim(tmin, tmax)
        self.axContour.set_ylim(fmin, fmax)
        self.axTime.set_xlim(tmin, tmax)
        self.axFreq.set_ylim(fmin, fmax)

        
        self.axFreq.get_yaxis().set_label_text("Frequency (Hz)")
        self.axTime.get_xaxis().set_label_text("time (s)")
        
        

        #r.plot(r.density(abs(transpose(X)).flatten().tolist()), xlab="Amplitude", ylab="density", type="l")
        #r.plot(r.density(abs(transpose(X)).flatten().tolist()))

        #sd = np.std(abs(transpose(X)))
        #m = np.mean(abs(transpose(X)))

        #norm = Normalize(vmin=np.min(abs(transpose(X))), vmax=np.max(abs(transpose(X))), clip=True)
        norm = Normalize(vmin=0, vmax=self.zmax)

        
        #CS = self.axContour.contourf(tX, fX, abs(transpose(X)), cmap=cm.get_cmap("jet"), norm=norm, extend="neither") # log(abs(transpose(X))+1))
        indT = where((array(tX) >= tmin)*(array(tX) <= tmax))[0]
        indF = where((array(fX) >= fmin)*(array(fX) <= fmax))[0]
        CS = self.axContour.imshow(abs(transpose(X[indT[0]:indT[-1], indF[0]:indF[-1]])),  
                                  interpolation = "bilinear", aspect='auto', 
                                extent=[tX[indT[0]], tX[indT[-1]], fX[indF[0]], fX[indF[-1]]], 
                                    cmap=cm.get_cmap("jet"), norm=norm, origin="lower") # 

        
        
        self.axTime.plot(t, signal)
        self.axFreq.plot(-abs(fft(signal)[0:((signal.size+1)/2)]), f)

        self.axContour.figure.colorbar(CS, use_gridspec=True, cax=self.axColorbar) #,    
   
   
        if self.computeNormalChk.isChecked():                
                
            # APPROXIMATION DE LA MULTINORMAL                
            #indMax = concatenate(where(abs(X) == np.max(np.max(abs(X)))))
            indMax = concatenate(where(abs(X) == np.max(abs(X))))
            
            y = abs(X)[indMax[0], :]
            x = abs(X)[:, indMax[1]]
    
            y1 = zeros(len(y))
            y2 = zeros(len(y))
            
            y1tmp = y[:(indMax[1]+1)][::-1] # [::-1] ==> reverse
            y2tmp = y[indMax[1]:]
    
            y1[:len(y1tmp)] = y1tmp
            y2[:len(y2tmp)] = y2tmp
    
            y12 = y1 + y2        
            y12[0] /= 2.0
            intY12 = cumtrapz(y12)
            
            indYGreater = where(intY12 >= 0.6827*intY12[-1])[0][0]
    
            sigmaY = indYGreater - (intY12[indYGreater] - 0.6827*intY12[-1])/(intY12[indYGreater] - intY12[indYGreater-1])
    
    
    
    
            x1 = zeros(len(x))
            x2 = zeros(len(x))
            
            x1tmp = x[:(indMax[0]+1)][::-1] # [::-1] ==> reverse
            x2tmp = x[indMax[0]:]
    
            x1[:len(x1tmp)] = x1tmp
            x2[:len(x2tmp)] = x2tmp
    
            x12 = x1 + x2        
            x12[0] /= 2.0
            intX12 = cumtrapz(x12)
            
            indXGreater = where(intX12 >= 0.6827*intX12[-1])[0][0]
    
            sigmaX = indXGreater - (intX12[indXGreater] - 0.6827*intX12[-1])/(intX12[indXGreater] - intX12[indXGreater-1])

    
            # OPTIMISATION DE LA MULTINORMALE
            from gaussfitter import gaussfit
            fitParams = gaussfit(abs(X), params=[np.max(np.max(abs(X))), indMax[1], indMax[0], sigmaX, sigmaY, 0.0], vheight=0)[1:]
        
        
            
            angleVar = arange(0, 2.0*pi*100 +1)/100.0
    
            muFO    = fX[0] + fitParams[1]*(fX[1]-fX[0])
            muTO    = tX[0] + fitParams[2]*(tX[1]-tX[0]) 
            sigmaYO = fitParams[4] #*(fX[1]-fX[0]) 
            sigmaXO = fitParams[3] #*(tX[1]-tX[0]) 
            xEllipseO = sigmaXO*cos(angleVar)
            yEllipseO = sigmaYO*sin(angleVar)
    
    
            rotAngle = -np.radians(fitParams[5])     
    
            
            xEllipseOR = xEllipseO*cos(rotAngle) - yEllipseO*sin(rotAngle)
            yEllipseOR = xEllipseO*sin(rotAngle) + yEllipseO*cos(rotAngle)
            
            self.axContour.plot(muTO + xEllipseOR*(tX[1]-tX[0]) , muFO + yEllipseOR*(fX[1]-fX[0]) , 'k--', linewidth=2)
    
    
            xl = sigmaXO*array([-1.0, 1.0, 0, 0]) 
            yl = sigmaYO*array([0, 0, -1.0, 1.0])
            
            xlR = xl*cos(rotAngle) - yl*sin(rotAngle)
            ylR = xl*sin(rotAngle) + yl*cos(rotAngle)        
    
            self.axContour.plot(xlR[:2]*(tX[1]-tX[0]) + muTO, ylR[:2]*(fX[1]-fX[0]) + muFO, 'k--', linewidth=2)
            self.axContour.plot(xlR[2:]*(tX[1]-tX[0]) + muTO, ylR[2:]*(fX[1]-fX[0]) + muFO, 'k--', linewidth=2)


        if self.computeRegChk.isChecked():  
            Y = abs(transpose(X[indT[0]:indT[-1], indF[0]:indF[-1]]))
            T = tX[indT[0]:indT[-1]]
            F = fX[indF[0]:indF[-1]]
            
              
            """
            ind = where((T >= float(self.beforeEventWdt.text()))*(T <= T[-1] - float(self.afterEventWdt.text())))[0]            
            
            regx = T[ind]
            regy = F[np.argmax(Y , axis=0)][ind]              
            regw = np.max(Y , axis=0)[ind]   
                        
            z = np.polyfit(regx, regy, deg=1, w=regw)
            """
            
            ind = where((T >= float(self.beforeEventWdt.text()))*(T <= T[-1] - float(self.afterEventWdt.text())))[0]            
            
            regx = T[ind]
            regy = []
            for i in ind:
                regy.append(trapz(F*Y[:, i], F)/trapz(Y[:, i], F))   
                
            z = np.polyfit(regx, regy, deg=1)            
            
            
            print "1:", len(T), T
            print "2:", len(F), F
            #print "2.5:", len(regw), regw
            print "3:", z          
            print 
            #print "4:", len(F[np.argmax(Y , axis=0)]), F[np.argmax(Y , axis=0)]

            #########################
            # SKEW TEST
            #from scipy.stats import skew           
            #self.axTime.cla()
            #self.axTime.get_yaxis().set_visible(True)
            #self.axTime.plot(T, skew(Y , axis=0))
            #self.axTime.plot(T, T*0)
            #self.axTime.set_xlim(min(T), max(T))
            #############################

            #########################
            # POWER TEST        
            #self.axTime.cla()
            #self.axTime.get_yaxis().set_visible(True)
            #self.axTime.plot(T, trapz(Y , axis=0))
            #self.axTime.plot(T, T*0)
            #self.axTime.set_xlim(min(T), max(T))
            #############################
            
            
            self.axContour.plot(regx, regy, 'w--', linewidth=2)
            self.axContour.plot(T, z[0]*T+z[1], 'm--', linewidth=2)
        
        self.spectogramPlot.draw()




    def montageChanged(self, Id):
        if Id:        
            montage = self.MainWin.reader.IRecordingMontage.GetReformattingMontage(Id-1)
        else:
            montage = self.MainWin.reader.IRecordingMontage
            
        self.channelCombo.clear()
        for i in range(montage.GetChannelCount()):
            self.channelCombo.addItem(montage.GetChannelLabel(i))






         