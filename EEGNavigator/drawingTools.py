# -*- coding: utf-8 -*-
"""
Created on Wed Jun 27 15:43:08 2012

@author: REVESTECH
"""
from PyQt4.QtCore import *
from PyQt4.QtGui import *

import PyQt4.Qwt5 as Qwt
import numpy as np

from scipy import arange

from PolyConst import stagesColor, stagesAbbrev

class MultiCanalDataPlot(Qwt.QwtPlot):

    def __init__(self, *args):
        Qwt.QwtPlot.__init__(self, *args)

        self.setCanvasBackground(Qt.white)

        self.curves = {}
        self.eventDisps = []       
        self.stageDisps = []         
        self.channelUpperLim = {} 
        self.channelLowerLim = {} 

        # On cache l'axe des y
        self.enableAxis(Qwt.QwtPlot.yLeft, False)
        self.enableAxis(Qwt.QwtPlot.xBottom, False)
        self.setMargin(0)
        self.plotLayout().setCanvasMargin(0)        
        

        self.totalDisplayHeight = 260.0 # mm     
        self.eventBandWidth     =   5.0 # mm
        
        self.eventColors        = {}


    def setSignals(self, signalInfo):
        
        # TODO: Appliquer une conversion entre les coordonnées de QwtPlot et 
        # les dimensions physiques affichées
             
        #print self.canvas().height()/self.canvas().physicalDpiY()*25.4    
        #print self.canvas().height()/self.canvas().logicalDpiY()*25.4              
        #print self.canvas().width()/self.canvas().physicalDpiX()*25.4    
        #print self.canvas().widthMM()    
        #print self.canvas().heightMM()    

        for curve in self.curves:
            self.curves[curve].detach()
        
        self.curves = {}          

        for record in signalInfo.records :            
            curve = Qwt.QwtPlotCurve(record.name)    
            linePen = QPen()
            linePen.setColor(record.sigColor)
            curve.setPen(linePen)                
            curve.attach(self)
            self.curves[str(record.name)] = curve    


    def updateSignals(self, signalInfo, signalsData, reader, start, stop):
     
        # TODO: Appliquer une conversion entre les coordonnées de QwtPlot et 
        # les dimensions physiques affichées
             
        #print self.canvas().height()/self.canvas().physicalDpiY()*25.4    
        #print self.canvas().height()/self.canvas().logicalDpiY()*25.4              
        #print self.canvas().width()/self.canvas().physicalDpiX()*25.4    
        #print self.canvas().widthMM()    
        #print self.canvas().heightMM()         

        height = self.totalDisplayHeight - self.eventBandWidth  # mm

        sigSlotsMax = 0.0
        for record in signalInfo.records :            
            sigSlotsMax += record.space
              
        sigSlots = 0.0
        for record in signalInfo.records :            
            curve =  self.curves[str(record.name)]       
            time = arange(start, stop, 1.0/signalsData[str(record.name)].samplingRate)
            curve.setData(time, signalsData[str(record.name)].signal/record.sensitivity + (sigSlotsMax-sigSlots-0.5*record.space)/sigSlotsMax*height )        

            self.channelUpperLim[str(record.name)] = (sigSlotsMax-sigSlots)/sigSlotsMax*height
            self.channelLowerLim[str(record.name)] = (sigSlotsMax-sigSlots-record.space)/sigSlotsMax*height 
            
            sigSlots += record.space

        self.setAxisScale(Qwt.QwtPlot.yLeft, 0.0, self.totalDisplayHeight)
        self.setAxisScale(Qwt.QwtPlot.xBottom, start, stop) 


    def setPageFrontiers(self, stageEvents, start, stop):   
        
        linePen = QPen(Qt.DashLine)
        linePen.setWidth(3)  
        linePen.setColor(QColor("grey"))
        
        i = 1
        for event in stageEvents:
            if event.startTime > start :
                curve = Qwt.QwtPlotCurve("pageSeparator" + str(i))       
                
                curve.setPen(linePen)                
                
                curve.attach(self)
                curve.setData([event.startTime, event.startTime], [-10, 1000])        
                self.curves["pageSeparator" + str(i)] = curve


        for stageDisp in self.stageDisps:
            stageDisp.detach()
        
        self.stageDisps = []            
        
        for event in stageEvents:                       
            stageDisp = StageDrawItem(event, self.totalDisplayHeight - self.eventBandWidth, 
                                      self.totalDisplayHeight, stagesColor[event.name], 
                                        stagesAbbrev[event.name])    
            stageDisp.attach(self)   
            self.stageDisps.append(stageDisp)



    def drawEvents(self, events, start, stop):   

        # Stages are not to be shown by boxes. (No need, stage events filtered out by the claler.)
        #events = filter(lambda e: e.groupName != "Stage", events)

        for eventDisp in self.eventDisps:
            eventDisp.detach()

        self.eventDisps = []            
        
        eventTypes = np.unique([e.name for e in events])        
        
        for eventType in eventTypes :
            if not eventType in self.eventColors :
                self.eventColors[eventType] = "black"        
        
        for event in events:           
            if str(event.channel) in self.channelUpperLim :               
                eventDisp = EventDrawItem(event, self.channelLowerLim[str(event.channel)], 
                                          self.channelUpperLim[str(event.channel)],
                                          self.eventColors[event.name])    
                eventDisp.attach(self)   
                self.eventDisps.append(eventDisp)
        #self.update()#(self.rect())



class StageDrawItem(Qwt.QwtPlotItem):
    def __init__(self, event, bottom, top, color, label ="W"):
        super(StageDrawItem, self).__init__()
        self.pen            = QPen(Qt.black)
        self.brush          = QBrush(color)
        self.rect           = QRectF(event.startTime, bottom, event.timeLength, top-bottom)    
        self.event          = event
        self.label          = label 


    def draw(self, painter, xMap, yMap, canvasRect ):
        
        drawRect = QRectF()
        
        drawRect.setLeft(   xMap.transform(self.rect.left()    ) )
        drawRect.setRight(  xMap.transform(self.rect.right()   ) )
        drawRect.setTop(    yMap.transform( self.rect.top()    ) )
        drawRect.setBottom( yMap.transform( self.rect.bottom() ) )

        drawRect = drawRect.normalized()
         
         
        painter.setPen( self.pen )
        painter.setBrush( self.brush )
        painter.drawRect( drawRect )
        
        labelWidget = QStaticText(self.label)              
        
        painter.drawStaticText ((drawRect.right()+drawRect.left()-labelWidget.size().width())/2.0,  
                                (drawRect.top()+drawRect.bottom()-labelWidget.size().height())/2.0, 
                                labelWidget)       




class EventDrawItem(Qwt.QwtPlotItem):
    def __init__(self, event, bottom, top, color, hasBorder = True):
                
        super(EventDrawItem, self).__init__()
        if hasBorder :        
            self.pen            = QPen(Qt.black)
        else:
            self.pen            = QPen(QColor(color))
            
        brushColor          = QColor(color)
        brushColor.setAlpha(50)
        self.brush          = QBrush(brushColor)
        self.rect           = QRectF(event.startTime, bottom, event.timeLength, top-bottom)    
        self.event          = event
        self.hasBorder      = hasBorder

    def draw(self, painter, xMap, yMap, canvasRect ):
        
        drawRect = QRectF()
        
        drawRect.setLeft(   xMap.transform(self.rect.left()   ) )
        drawRect.setRight(  xMap.transform(self.rect.right()  ) )
        drawRect.setTop(    yMap.transform(self.rect.top()    ) )
        drawRect.setBottom( yMap.transform(self.rect.bottom() ) )

        drawRect = drawRect.normalized()
         
        painter.setPen( self.pen )
        painter.setBrush( self.brush )
        painter.drawRect( drawRect )





class DrawRectWithText(Qwt.QwtPlotItem):
    def __init__(self, start, duration, bottom, top, fillColor, borderColor, label =""):
        super(DrawRectWithText, self).__init__()
        self.pen            = QPen(borderColor)
        self.brush          = QBrush(fillColor)
        self.rect           = QRectF(start, bottom, duration, top-bottom)    
        self.label          = label 


    def draw(self, painter, xMap, yMap, canvasRect ):
        
        drawRect = QRectF()
        
        drawRect.setLeft(   xMap.transform(self.rect.left()    ) )
        drawRect.setRight(  xMap.transform(self.rect.right()   ) )
        drawRect.setTop(    yMap.transform( self.rect.top()    ) )
        drawRect.setBottom( yMap.transform( self.rect.bottom() ) )

        drawRect = drawRect.normalized()
         
         
        painter.setPen( self.pen )
        painter.setBrush( self.brush )
        painter.drawRect( drawRect )
        
        labelWidget = QStaticText(self.label)  
            
        painter.setPen(QPen("black"))
        painter.drawStaticText ((drawRect.right()+drawRect.left()-labelWidget.size().width())/2.0,  
                                (drawRect.top()+drawRect.bottom()-labelWidget.size().height())/2.0, 
                                labelWidget)       
