# -*- coding: utf-8 -*-
"""
Created on Wed Jun 27 15:39:38 2012

@author: REVESTECH
"""


from PyQt4.QtCore import *
from PyQt4.QtGui import *

stagesColor =  {"Sleep stage ?"     : QColor(255, 255, 255), 
                "Sleep stage W"     : QColor(  0, 255,   0), 
                "Sleep stage R"     : QColor(255,   0,   0), 
                "Sleep stage N"     : QColor(125, 155, 155), 
                "Sleep stage 1"     : QColor(  0, 255, 255), 
                "Sleep stage N1"    : QColor(  0, 255, 255), 
                "Sleep stage 2"     : QColor(255, 255,   0), 
                "Sleep stage N2"    : QColor(255, 255,   0), 
                "Sleep stage 3"     : QColor(255,   0, 255), 
                "Sleep stage N3"    : QColor(255,   0, 255), 
                "Sleep stage 4"     : QColor(255,   0, 155)}


stagesAbbrev=  {"Sleep stage ?"     : "U", 
                "Sleep stage W"     : "W", 
                "Sleep stage R"     : "R", 
                "Sleep stage N"     : "N", 
                "Sleep stage 1"     : "1", 
                "Sleep stage N1"    : "N1", 
                "Sleep stage 2"     : "2", 
                "Sleep stage N2"    : "N2", 
                "Sleep stage 3"     : "3", 
                "Sleep stage N3"    : "N3", 
                "Sleep stage 4"     : "4"}
 


stageDisplayName = {"Sleep stage ?"     : "undetermined", 
                    "Sleep stage W"     : "wake", 
                    "Sleep stage R"     : "REM", 
                    "Sleep stage N"     : "NREM", 
                    "Sleep stage 1"     : "1", 
                    "Sleep stage N1"    : "N1", 
                    "Sleep stage 2"     : "2", 
                    "Sleep stage N2"    : "N2", 
                    "Sleep stage 3"     : "3", 
                    "Sleep stage N3"    : "N3", 
                    "Sleep stage 4"     : "4"}   
                        
                        
                    

                     
                    
                    
                    
           