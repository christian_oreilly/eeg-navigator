# -*- coding: utf-8 -*-

#!/usr/bin/env python

# The Python version of Qwt-5.0.0/examples/data_plot

import  os, cPickle, copy

#from FIRfilter import FIRFilter_PassBand

from PyQt4.QtCore import *
from PyQt4.QtGui import *
from PyQt4.QtWebKit import *

from spyndle.miscellaneous import channelType, Filter
from spyndle.miscellaneous import computeDreamCycles

from sleepCyclesDlg import changeCycleDefinitionDlg
from drawingTools import EventDrawItem, DrawRectWithText
from PolyConst import stagesColor

from sigDisplayInfo import SigDisplayInfo

from scipy import where, array, log10, arange, hamming, fft, linspace, transpose, tile, log
from scipy.fftpack import fftfreq

import PyQt4.Qwt5 as Qwt

import datetime
from datetime import timedelta

from math import floor, ceil



from reportGenerator import SpindleReportGenerator




class showSpingleReportDlg(QDialog):
    def __init__(self, parent):
        QDialog.__init__(self, parent)

    
        self.setWindowTitle('Spindle generation dialog')
        self.box = QGridLayout()
        self.setLayout(self.box)  
        self.MainWin = parent
        #self.resize(700,150)

        
        self.generateButton  = QPushButton("Generate!")        
        self.generateButton.clicked.connect(self.generateReport)     
        
        
        self.closeButton  = QPushButton("Close")        
        self.closeButton.clicked.connect(self.accept)     

        
        self.channelCombo = QComboBox()     
        self.montageCombo = QComboBox()     

        self.montageCombo.currentIndexChanged.connect(self.montageChanged)    

        self.montageCombo.addItem(self.MainWin.reader.IRecordingMontage.GetMontageName())        
        for i in range(self.MainWin.reader.IRecordingMontage.GetReformattingMontageCount()):
            self.montageCombo.addItem(self.MainWin.reader.IRecordingMontage.GetReformattingMontage(i).GetMontageName())
        
        self.montageCombo.setCurrentIndex(0)         


        self.view = QWebView(parent)
     

        self.baseFileNameEdt = QLineEdit(self.MainWin.fileName[0:-4] + '_Fuseau_test.htm')     





        self.cycleDefinitionLbl = QLabel(self.MainWin.sleepCycleDef.type, self)
        self.cycleDefinitionLbl.setFrameStyle(QFrame.Panel | QFrame.Sunken)
        self.cycleDefChangeBtn = QPushButton("change")
                
        self.cycleDefChangeBtn.clicked.connect(self.changeCycleDefinition)                   
      
        self.cycleDefinitionLayout = QHBoxLayout()
        self.cycleDefinitionLayout.addWidget(QLabel("Cycle definition: "))   
        self.cycleDefinitionLayout.addWidget(self.cycleDefinitionLbl)   
        self.cycleDefinitionLayout.addWidget(self.cycleDefChangeBtn)   
        
        self.eventNameEdt = QLineEdit("Fuseau")
       
        
        self.box.addWidget(self.view , 1, 1, 1, 6) 
        self.box.addWidget(QLabel("Base name:"), 2, 1) 
        self.box.addWidget(self.baseFileNameEdt,    2, 2, 1, 5) 
        self.box.addWidget(QLabel("Event name:"), 3, 1) 
        self.box.addWidget(self.eventNameEdt,    3, 2) 
        self.box.addWidget(QLabel("Montage:"), 3, 3) 
        self.box.addWidget(self.montageCombo,    3, 4) 
        self.box.addWidget(QLabel("Channel:"), 3, 5) 
        self.box.addWidget(self.channelCombo,    3, 6) 
        self.box.addLayout(self.cycleDefinitionLayout, 4, 3, 1, 2) 
        self.box.addWidget(self.generateButton, 4, 5) 
        self.box.addWidget(self.closeButton,    4, 6) 


    def montageChanged(self, Id):
        if Id:        
            montage = self.MainWin.reader.IRecordingMontage.GetReformattingMontage(Id-1)
        else:
            montage = self.MainWin.reader.IRecordingMontage
            
        self.channelCombo.clear()
        for i in range(montage.GetChannelCount()):
            self.channelCombo.addItem(montage.GetChannelLabel(i))


    def accept(self):        
        QDialog.accept(self)



    def generateReport(self):
        #fname = QFileDialog.getSaveFileName(self, 'Save report as...', self.MainWin.persistenceClass.sigPath)
        #        
        #self.MainWin.persistenceClass.sigPath = os.path.split(str(fname))[0]   
        
                
        fname = str(self.baseFileNameEdt.text())
        generator = SpindleReportGenerator()
        generator.generate(fname, self.MainWin, str(self.montageCombo.currentText()),
                           str(self.channelCombo.currentText()), str(self.eventNameEdt.text()))

        self.view.load(QUrl(fname))
        self.view.show()   
        

    def changeCycleDefinition(self):
        dlg = changeCycleDefinitionDlg(self, self.MainWin.sleepCycleDef)
        dlg.exec_()   
        self.cycleDefinitionLbl.setText(self.MainWin.sleepCycleDef.type)

                
            
            
  
        
        
        
        











class yScaleDraw(Qwt.QwtScaleDraw):
    def label(self, stade):
        stadeLst = ['U', '4', "3", "2", "1", "R", "W"]  
        return Qwt.QwtText(stadeLst[int(stade)])


class xScaleDraw(Qwt.QwtScaleDraw):        
    def label(self, secs):
        return Qwt.QwtText(str(  (int(secs/3600.0)-1)%24 + 1    ))


class showHypnogramToolDlg(QDialog):
    def __init__(self, parent, stageEventLst, sleepCycleDef):
        QDialog.__init__(self, parent)

        self.cyclesRect = []
        self.sleepCycleDef = sleepCycleDef

        self.setWindowTitle('Hypnogram plotting tools')
        self.box = QGridLayout()
        self.setLayout(self.box)  
        self.MainWin = parent
        self.resize(700,150)

        self.plotter = Qwt.QwtPlot(self)
        self.box.addWidget(self.plotter, 1, 1) 
        self.plotter.setTitle("Hypnogram")
        self.plotter.setCanvasBackground(Qt.white)
                
        self.stageEventLst = stageEventLst
        
        # 6 5 4 3 2 1 0 
        # W R 1 2 3 4 U       
        
        self.stagesY = {'Unstaged'  : 0.0, 
                        'StageU'    : 0.0, 
                        'WAKE'      : 6.0, 
                        "REM"       : 5.0, 
                        "Stage1"    : 4.0, 
                        "Stage2"    : 3.0, 
                        "Stage3"    : 2.0}        
        
        self.resolutionLayout = QHBoxLayout()
        
        
        self.pageRadioBtn = QRadioButton("page", self)
        self.pageRadioBtn.setChecked(True)
        self.cycleRadioBtn = QRadioButton("cycle", self)
        self.cycleDefinitionLbl = QLabel(self)
        self.cycleDefinitionLbl.setText(self.sleepCycleDef.type)
        self.cycleDefinitionLbl.setFrameStyle(QFrame.Panel | QFrame.Sunken)
        self.cycleDefChangeBtn = QPushButton("change")
                
        self.cycleDefChangeBtn.clicked.connect(self.changeCycleDefinition)                   
                
                
        
        self.resolutionLayout.addWidget(QLabel("Resolution: "))
        self.resolutionLayout.addWidget(self.pageRadioBtn)
        self.resolutionLayout.addWidget(self.cycleRadioBtn)
        self.resolutionLayout.addWidget(QLabel("Cycle definition: "))   
        self.resolutionLayout.addWidget(self.cycleDefinitionLbl)      
        self.resolutionLayout.addWidget(self.cycleDefChangeBtn)        
        
        self.box.addLayout(self.resolutionLayout, 2, 1)          
        
        self.dreamCycles = computeDreamCycles(self.stageEventLst, self.sleepCycleDef)   
        self.plotCycles()

        
        self.show()
                
        
        
    def plotCycles(self): 

        for cycleRect in self.cyclesRect:
            cycleRect.detach()        
        
        self.cyclesRect = []
        for dreamCycle, i in zip(self.dreamCycles, range(len(self.dreamCycles))):
            colorREM  = QColor("red")  
            rect = DrawRectWithText(dreamCycle.timeStartREM, dreamCycle.timeDurationREM, 6.25, 7.0, colorREM, colorREM, "") #"REM" + str(i+1))            
            rect.attach(self.plotter)   
            self.cyclesRect.append(rect)            
        
            colorNREM = QColor("cyan")
            rect = DrawRectWithText(dreamCycle.timeStartNREM, dreamCycle.timeDurationNREM, 6.25, 7.0, colorNREM, colorNREM, str(i+1)) #"NREM" + str(i+1))            
            rect.attach(self.plotter)   
            self.cyclesRect.append(rect)   

        
        
    def show(self):
        self.eventDisps = []
        
        for event in self.stageEventLst:           
            color = stagesColor[str(event.name)]
            event.color = qRgb(color.red(), color.green(), color.blue())     
            eventDisp = EventDrawItem(event, 0, self.stagesY[str(event.name)], False)           
            eventDisp.attach(self.plotter)   
            self.eventDisps.append(eventDisp)
            
            
        start = min([stage.startTime for stage in self.stageEventLst])
        end   = max([stage.startTime + stage.timeLength for stage in self.stageEventLst])
           
        self.plotter.setAxisScale(Qwt.QwtPlot.yLeft, 0.0, 6.0)
        self.plotter.setAxisScale(Qwt.QwtPlot.xBottom, start, end)            

        self.plotter.setAxisScaleDraw(Qwt.QwtPlot.yLeft, yScaleDraw())
        scaleDivY = Qwt.QwtScaleDiv(0.0, 7.0, [], [], [0.0, 1.0, 2.0, 3.0, 4.0, 5.0, 6.0])        
        self.plotter.setAxisScaleDiv(Qwt.QwtPlot.yLeft, scaleDivY)



        self.plotter.setAxisScaleDraw(Qwt.QwtPlot.xBottom, xScaleDraw())
        scaleDivY = Qwt.QwtScaleDiv(start, end, [], [], arange(floor(start/3600.0), ceil(end/3600.0)+1)*3600)        
        self.plotter.setAxisScaleDiv(Qwt.QwtPlot.xBottom, scaleDivY)


        self.plotter.replot()  
        
        
        #curve = Qwt.QwtPlotCurve("freq. response")    
        #linePen = QPen()
        #linePen.setColor(QColor.black)
        #curve.setPen(linePen)                
        #curve.attach(self.plotter)
        
        #curve.setData(w[1:-1], SNR[1:-1]) # On exclut la fréquence nulle puisqu'elle 
                                        # pose problème sur échelle logarithmique       
        
        
        
        

    def changeCycleDefinition(self):
        dlg = changeCycleDefinitionDlg(self, self.sleepCycleDef)
        dlg.exec_()   
        self.cycleDefinitionLbl.setText(self.sleepCycleDef.type)
        
        self.dreamCycles = computeDreamCycles(self.stageEventLst, self.sleepCycleDef)   
        self.plotCycles()

        self.plotter.replot()  
                
        


        
        
        
        
        
        
        











#
#
#class showHypnogramToolDlg_test(QDialog):
#    def __init__(self, parent, stageEventLst, sleepCycleDef):
#        QDialog.__init__(self, parent)
#
#        self.cyclesRect = []
#        self.sleepCycleDef = sleepCycleDef
#
#        self.setWindowTitle('Hypnogram plotting tools')
#        self.box = QGridLayout()
#        self.setLayout(self.box)  
#        self.MainWin = parent
#        self.resize(700,150)
#
#        self.plotter = Qwt.QwtPlot(self)
#        self.box.addWidget(self.plotter, 1, 1) 
#        self.plotter.setTitle("Hypnogram")
#        self.plotter.setCanvasBackground(Qt.white)
#                
#        self.stageEventLst = stageEventLst
#        
#        # 6 5 4 3 2 1 0 
#        # W R 1 2 3 4 U       
#        
#        self.stagesY = {'Unstaged'  : 0.0, 
#                        'StageU'    : 0.0, 
#                        'WAKE'      : 6.0, 
#                        "REM"       : 5.0, 
#                        "Stage1"    : 4.0, 
#                        "Stage2"    : 3.0, 
#                        "Stage3"    : 2.0}        
#        
#        self.resolutionLayout = QHBoxLayout()
#        
#        
#        self.pageRadioBtn = QRadioButton("page", self)
#        self.pageRadioBtn.setChecked(True)
#        self.cycleRadioBtn = QRadioButton("cycle", self)
#        self.cycleDefinitionLbl = QLabel(self)
#        self.cycleDefinitionLbl.setText(self.sleepCycleDef.type)
#        self.cycleDefinitionLbl.setFrameStyle(QFrame.Panel | QFrame.Sunken)
#        self.cycleDefChangeBtn = QPushButton("change")
#                
#        self.cycleDefChangeBtn.clicked.connect(self.changeCycleDefinition)                   
#                
#                
#        
#        self.resolutionLayout.addWidget(QLabel("Resolution: "))
#        self.resolutionLayout.addWidget(self.pageRadioBtn)
#        self.resolutionLayout.addWidget(self.cycleRadioBtn)
#        self.resolutionLayout.addWidget(QLabel("Cycle definition: "))   
#        self.resolutionLayout.addWidget(self.cycleDefinitionLbl)      
#        self.resolutionLayout.addWidget(self.cycleDefChangeBtn)        
#        
#        self.box.addLayout(self.resolutionLayout, 2, 1)          
#        
#        self.dreamCycles = computeDreamCycles(self.stageEventLst, self.sleepCycleDef)   
#        self.plotCycles()
#
#        
#        self.show()
#                
#        
#        
#    def plotCycles(self): 
#
#        for cycleRect in self.cyclesRect:
#            cycleRect.detach()        
#        
#        self.cyclesRect = []
#        for dreamCycle, i in zip(self.dreamCycles, range(len(self.dreamCycles))):
#            colorREM  = QColor("red")  
#            rect = DrawRectWithText(dreamCycle.timeStartREM, dreamCycle.timeDurationREM, 6.25, 7.0, colorREM, colorREM, "") #"REM" + str(i+1))            
#            rect.attach(self.plotter)   
#            self.cyclesRect.append(rect)            
#        
#            colorNREM = QColor("cyan")
#            rect = DrawRectWithText(dreamCycle.timeStartNREM, dreamCycle.timeDurationNREM, 6.25, 7.0, colorNREM, colorNREM, str(i+1)) #"NREM" + str(i+1))            
#            rect.attach(self.plotter)   
#            self.cyclesRect.append(rect)   
#
#
#        sampleStart = [45, 195, 357, 548]
#        sampleEnd   = [194, 356, 547, 755]
#        
#        colLst  = [QColor("red"), QColor("blue"), QColor("red"), QColor("blue")]          
#        for start, stop, col in zip(sampleStart, sampleEnd, colLst):
#
#            duration  = self.MainWin.pageTimeStart[stop-1]+30.0-self.MainWin.pageTimeStart[start-1]
#            rect = DrawRectWithText(self.MainWin.pageTimeStart[start-1], duration, 7.25, 8.0, col, col, "") #"REM" + str(i+1))            
#            rect.attach(self.plotter)   
#            self.cyclesRect.append(rect)            
#        
#            #colorNREM = QColor("cyan")
#            #rect = DrawRectWithText(dreamCycle.timeStartNREM, dreamCycle.timeDurationNREM, 7.25, 8.0, colorNREM, colorNREM, str(i+1)) #"NREM" + str(i+1))            
#            #rect.attach(self.plotter)   
#            #self.cyclesRect.append(rect)   
#
#        
#        
#    def show(self):
#        self.eventDisps = []
#        
#        for event in self.stageEventLst:           
#            color = stagesColor[str(event.name)]
#            event.color = qRgb(color.red(), color.green(), color.blue())     
#            eventDisp = EventDrawItem(event, 0, self.stagesY[str(event.name)], False)           
#            eventDisp.attach(self.plotter)   
#            self.eventDisps.append(eventDisp)
#            
#            
#        start = min([stage.startTime for stage in self.stageEventLst])
#        end   = max([stage.startTime + stage.timeLength for stage in self.stageEventLst])
#           
#        self.plotter.setAxisScale(Qwt.QwtPlot.yLeft, 0.0, 6.0)
#        self.plotter.setAxisScale(Qwt.QwtPlot.xBottom, start, end)            
#
#        self.plotter.setAxisScaleDraw(Qwt.QwtPlot.yLeft, yScaleDraw())
#        scaleDivY = Qwt.QwtScaleDiv(0.0, 8.0, [], [], [0.0, 1.0, 2.0, 3.0, 4.0, 5.0, 6.0])        
#        self.plotter.setAxisScaleDiv(Qwt.QwtPlot.yLeft, scaleDivY)
#
#
#
#        self.plotter.setAxisScaleDraw(Qwt.QwtPlot.xBottom, xScaleDraw())
#        scaleDivY = Qwt.QwtScaleDiv(start, end, [], [], arange(floor(start/3600.0), ceil(end/3600.0)+1)*3600)        
#        self.plotter.setAxisScaleDiv(Qwt.QwtPlot.xBottom, scaleDivY)
#
#
#        self.plotter.replot()  
#        
#        
#        #curve = Qwt.QwtPlotCurve("freq. response")    
#        #linePen = QPen()
#        #linePen.setColor(QColor.black)
#        #curve.setPen(linePen)                
#        #curve.attach(self.plotter)
#        
#        #curve.setData(w[1:-1], SNR[1:-1]) # On exclut la fréquence nulle puisqu'elle 
#                                        # pose problème sur échelle logarithmique       
#        
#        
#        
#        
#
#    def changeCycleDefinition(self):
#        dlg = changeCycleDefinitionDlg(self, self.sleepCycleDef)
#        dlg.exec_()   
#        self.cycleDefinitionLbl.setText(self.sleepCycleDef.type)
#        
#        self.dreamCycles = computeDreamCycles(self.stageEventLst, self.sleepCycleDef)   
#        self.plotCycles()
#
#        self.plotter.replot()  
#                
#        


        
        









        
        
        


class setFiltersDlg(QDialog):
    def __init__(self, sigSignalRates, parent=None):
        QDialog.__init__(self, parent)
        self.setWindowTitle('Set signal dialog')
        self.MainWin = parent
        self.mainLayout = QGridLayout()     
        self.setLayout(self.mainLayout)         

        self.btypeLst = ["lowpass", "highpass", "bandpass", "bandstop"]
        self.ftypeLst = ["ellip", "butter", "cheby1", "cheby2", "bessel"]            


        self.filterBank = copy.deepcopy(self.MainWin.filterBank)

        self.createBoxLayout()
        

            
        buttonBox = QDialogButtonBox(QDialogButtonBox.Ok
                                      | QDialogButtonBox.Cancel)         
        
        buttonBox.accepted.connect(self.accept );  
        buttonBox.rejected.connect(self.reject );        
        
        
        
        self.addButton  = QPushButton("Add a filter")        
        self.addButton.clicked.connect(self.addAFilter)     
        
   

        
        self.controlLayout =  QHBoxLayout()                    
        self.controlLayout.addWidget(self.addButton, 1)
        self.controlLayout.addWidget(buttonBox, 2)


        self.mainLayout.addWidget(self.centralWidget, 1, 1)     
        self.mainLayout.addLayout(self.controlLayout, 2, 1)     

        self.sigSignalRates = sigSignalRates



    def createBoxLayout(self):
        
        self.centralWidget = QWidget(self)

        self.edtOrder = []
        self.lowCritFreqWidget = []
        self.highCritFreqWidget = []
        self.rpWidget = []
        self.rsWidget = []        
        
     
        self.btnGrpShowResponse = QButtonGroup(self) 
        self.btnGrpRemove       = QButtonGroup(self) 
        
        self.btnGrpShowResponse.buttonClicked.connect(self.showBtnClicked)
        self.btnGrpRemove.buttonClicked.connect(self.removeFilter)
        
        
        box = QGridLayout() 
        self.centralWidget.setLayout(box)
                      
        #box.addWidget(QLabel('Reponse type'), 1, 1) # IIR/FIR
        box.addWidget(QLabel('Channel type'), 1, 1) 
        box.addWidget(QLabel('btype'), 1, 2)
        box.addWidget(QLabel('ftype'), 1, 3)                
        box.addWidget(QLabel('order'), 1, 4)                
        box.addWidget(QLabel('low crit. freq.'), 1, 5)                           
        box.addWidget(QLabel('high crit. freq.'), 1, 6)                          
        box.addWidget(QLabel('ripple in BP'), 1, 7)                          
        box.addWidget(QLabel('attenuation in BS'), 1, 8)                          
        box.addWidget(QLabel('zero phase'), 1, 9)    
       
              
        for i, dispFilter in zip(range(2, len(self.filterBank)+2), self.filterBank) :
            
            ################### Channel type
            comboCtype = QComboBox() 
            comboCtype.id = i-2
            comboCtype.currentIndexChanged.connect(self.comboCtypeIndexChanged)            
            
            self.channelTypesLst = []
            for key in channelType:
                comboCtype.addItem(key)
                self.channelTypesLst.append(key)
                
                
            channelTypeLabel = [k for k,v in channelType.iteritems() if v == dispFilter.channelType][0]
            comboCtype.setCurrentIndex(where(array(self.channelTypesLst) == channelTypeLabel)[0][0])     
            
            
            
            

            box.addWidget(comboCtype, i, 1)            
            
            
            
            
            
            #################### btype
            comboBtype = QComboBox() 
            comboBtype.id = i-2
            comboBtype.currentIndexChanged.connect(self.comboBtypeIndexChanged)            
            comboBtype.addItem("lowpass")
            comboBtype.addItem("highpass")
            comboBtype.addItem("bandpass")
            comboBtype.setCurrentIndex(self.btypeLst.index(dispFilter.btype))            
            #comboBtype.addItem("bandstop")
            box.addWidget(comboBtype, i, 2)




            ###################### ftype
            comboFtype = QComboBox() 
            comboFtype.id = i-2
            comboFtype.currentIndexChanged.connect(self.comboFtypeIndexChanged)
            comboFtype.addItem('ellip')
            comboFtype.addItem('butter')
            comboFtype.addItem('cheby1')
            comboFtype.addItem('cheby2')
            comboFtype.addItem('bessel')
            comboFtype.setCurrentIndex(self.ftypeLst.index(dispFilter.ftype))            
            box.addWidget(comboFtype, i, 3)





            ####################### order
            self.edtOrder.append(QLineEdit(str(dispFilter.order)))
            self.edtOrder[i-2].id = i-2
            self.edtOrder[i-2].textEdited.connect(self.edtOrderEditingFinished )            
            box.addWidget(self.edtOrder[i-2], i, 4)
            
            
            
            validator = QDoubleValidator(0.0001, 100.0, 20, self)
            
            
            ######################  low_crit_freq
            self.lowCritFreqWidget.append(QLineEdit())      
            box.addWidget(self.lowCritFreqWidget[i-2], i, 5)
            self.lowCritFreqWidget[i-2].setValidator(validator)
            self.lowCritFreqWidget[i-2].id = i-2
            self.lowCritFreqWidget[i-2].textEdited.connect(self.lowCritFreqWidgetEditingFinished)                  
            if dispFilter.btype == "highpass" or dispFilter.btype == "bandpass" :
                self.lowCritFreqWidget[i-2].setText(str(dispFilter.low_crit_freq ))
            else:
                self.lowCritFreqWidget[i-2].setText("")
                self.lowCritFreqWidget[i-2].setEnabled(False)





            ######################  high_crit_freq      
            self.highCritFreqWidget.append(QLineEdit())            
            self.highCritFreqWidget[i-2].setValidator(validator)       
            self.highCritFreqWidget[i-2].id = i-2
            self.highCritFreqWidget[i-2].textEdited.connect(self.highCritFreqWidgetEditingFinished)  
            box.addWidget(self.highCritFreqWidget[i-2], i, 6)                 
            if dispFilter.btype == "lowpass" or dispFilter.btype == "bandpass" :
                self.highCritFreqWidget[i-2].setText(str(dispFilter.high_crit_freq ))                
            else:
                self.highCritFreqWidget[i-2].setText("")
                self.highCritFreqWidget[i-2].setEnabled(False)
                          




            ###################### rp
            self.rpWidget.append(QLineEdit())
            self.rpWidget[i-2].id = i-2
            self.rpWidget[i-2].textEdited.connect(self.rpWidgetEditingFinished)  
            box.addWidget(self.rpWidget[i-2], i, 7)     
            if dispFilter.ftype == "cheby1" or dispFilter.btype == "ellip" :
                self.rpWidget[i-2].setText(str(dispFilter.rp))
            else:
                self.rpWidget[i-2].setText("")
                self.rpWidget[i-2].setEnabled(False)
            
      
            
            
            
            ###################### rs      
            self.rsWidget.append(QLineEdit())   
            self.rsWidget[i-2].id = i-2
            self.rsWidget[i-2].textEdited.connect(self.rsWidgetEditingFinished)  
            box.addWidget(self.rsWidget[i-2], i, 8)     
            if  dispFilter.ftype == "cheby1" or dispFilter.ftype == "cheby2" or dispFilter.btype == "ellip" :
                self.rsWidget[i-2].setText(str(dispFilter.rs))
            else:
                self.rsWidget[i-2].setText("")
                self.rsWidget[i-2].setEnabled(False)           
                
                
                
 
                
            ####################### activate filtfilt
            ckbBox = QCheckBox()   
            ckbBox.id = i-2
            ckbBox.stateChanged.connect(self.ckbBoxStateChanged)              
            ckbBox.setChecked(dispFilter.useFiltFilt)
            box.addWidget(ckbBox, i, 9)  
                        
                        
      
                        
                        
            ####################### show frequency            
            button  = QPushButton("show frequency response")
            button.id = i-2
            box.addWidget(button, i, 10)
            self.btnGrpShowResponse.addButton(button, i)
      
      
      
      
      
            ####################### Remove
            button  = QPushButton("Remove")
            button.id = i-2
            box.addWidget(button, i, 11)
            self.btnGrpRemove.addButton(button, i)

#
#
#    def colorBtnClicked(self, button):
#        palette = button.palette()
#
#        newButtonColor = QColorDialog.getColor(button.palette().color(QPalette.Button), self)
#        #palette.setColor(QPalette.Button, newButtonColor)
#        #button.setPalette(palette)
#        #button.repaint()
#        #self.repaint()
#        button.setStyleSheet("* { background-color: rgb(" + str(newButtonColor.red()) + "," 
#                            + str(newButtonColor.green()) + "," + str(newButtonColor.blue()) + ") }")
#
#
    def accept(self):
        self.MainWin.filterBank = copy.deepcopy(self.filterBank)
        
        # Bug de QT? Quand l'utilisant clique sur OK après avoir modifier la valeur
        # d'un LineEdit, l'évènement editingFinished n'est pas lancé...
        # On contourne le problème....
        #for o, l, h, rp, rs, i in zip(self.edtOrder, self.lowCritFreqWidget, 
        #                              self.highCritFreqWidget, self.rpWidget, self.rsWidget, range(len(self.edtOrder))):
#
 #           self.MainWin.filterBank[i].order = int(o.text()) 
  #          self.MainWin.filterBank[i].low_crit_freq = float(l.text()) 
   #         self.MainWin.filterBank[i].high_crit_freq = float(h.text()) 
    #        if not self.MainWin.filterBank[i].rp is None:
     #           self.MainWin.filterBank[i].rp = float(rp.text()) 
      #      if not self.MainWin.filterBank[i].rs is None:
      #          self.MainWin.filterBank[i].rs = float(rs.text()) 
       # 
        #    self.MainWin.filterBank[i].update()    
        ######### Fin du contournement....
        
        cPickle.dump(self.MainWin.filterBank, file("FilterBankInfoClass.pck", "w"))            
        
        QDialog.accept(self)




    def reject(self):
        QDialog.reject(self)



    def comboCtypeIndexChanged(self, selectId):
        if hasattr(qApp.focusWidget(), 'id'):   
            self.filterBank[qApp.focusWidget().id].channelType  = channelType[self.channelTypesLst[selectId]]
            self.filterBank[qApp.focusWidget().id].samplingRate = self.sigSignalRates[self.channelTypesLst[selectId]]
            self.filterBank[qApp.focusWidget().id].update()

    def comboBtypeIndexChanged(self, selectId):
        if hasattr(qApp.focusWidget(), 'id'):
            self.filterBank[qApp.focusWidget().id].btype = self.btypeLst[selectId]
            
            if self.btypeLst[selectId] == "bandpass":
                if self.filterBank[qApp.focusWidget().id].high_crit_freq is None:
                    self.filterBank[qApp.focusWidget().id].high_crit_freq = 30.0
                    self.highCritFreqWidget[qApp.focusWidget().id].setText("30.0")
                self.highCritFreqWidget[qApp.focusWidget().id].setEnabled(True)

                if self.filterBank[qApp.focusWidget().id].low_crit_freq is None:                
                    self.filterBank[qApp.focusWidget().id].low_crit_freq = 0.5                
                    self.lowCritFreqWidget[qApp.focusWidget().id].setText("0.5")
                self.lowCritFreqWidget[qApp.focusWidget().id].setEnabled(True)
                
            elif self.btypeLst[selectId] == "lowpass":
                if self.filterBank[qApp.focusWidget().id].high_crit_freq is None:
                    self.filterBank[qApp.focusWidget().id].high_crit_freq = 30.0
                    self.highCritFreqWidget[qApp.focusWidget().id].setText("30.0")
                self.highCritFreqWidget[qApp.focusWidget().id].setEnabled(True)

                self.filterBank[qApp.focusWidget().id].low_crit_freq = None                
                self.lowCritFreqWidget[qApp.focusWidget().id].setText("")
                self.lowCritFreqWidget[qApp.focusWidget().id].setEnabled(False)
                

            elif self.btypeLst[selectId] == "highpass":
                self.filterBank[qApp.focusWidget().id].high_crit_freq = None
                self.highCritFreqWidget[qApp.focusWidget().id].setText("")
                self.highCritFreqWidget[qApp.focusWidget().id].setEnabled(False)

                if self.filterBank[qApp.focusWidget().id].low_crit_freq is None:                
                    self.filterBank[qApp.focusWidget().id].low_crit_freq = 0.5                
                    self.lowCritFreqWidget[qApp.focusWidget().id].setText("0.5")
                self.lowCritFreqWidget[qApp.focusWidget().id].setEnabled(True)
            
            self.filterBank[qApp.focusWidget().id].update()        

    def comboFtypeIndexChanged(self, selectId):
        if hasattr(qApp.focusWidget(), 'id'):
            
            self.filterBank[qApp.focusWidget().id].ftype = self.ftypeLst[selectId] 
            if (self.filterBank[qApp.focusWidget().id].ftype == "cheby1" or 
                        self.filterBank[qApp.focusWidget().id].ftype == "cheby2" or
                        self.filterBank[qApp.focusWidget().id].ftype == "ellip"):
                self.filterBank[qApp.focusWidget().id].rs = 80.0
                self.rsWidget[qApp.focusWidget().id].setText("80.0")
                self.rsWidget[qApp.focusWidget().id].setEnabled(True)
                
            if (self.filterBank[qApp.focusWidget().id].ftype == "cheby1" or 
                        self.filterBank[qApp.focusWidget().id].ftype == "ellip"):
                self.filterBank[qApp.focusWidget().id].rp = 1.0
                self.rpWidget[qApp.focusWidget().id].setText("1.0")
                self.rpWidget[qApp.focusWidget().id].setEnabled(True)

            if self.filterBank[qApp.focusWidget().id].ftype == "cheby2":
                self.filterBank[qApp.focusWidget().id].rp = None
                self.rpWidget[qApp.focusWidget().id].setText("")
                self.rpWidget[qApp.focusWidget().id].setEnabled(False)


                
            if (self.filterBank[qApp.focusWidget().id].ftype == "bessel" or 
                        self.filterBank[qApp.focusWidget().id].ftype == "butter"):
                self.filterBank[qApp.focusWidget().id].rp = None
                self.rpWidget[qApp.focusWidget().id].setText("")
                self.rpWidget[qApp.focusWidget().id].setEnabled(False)
                
                self.filterBank[qApp.focusWidget().id].rs = None
                self.rsWidget[qApp.focusWidget().id].setText("")
                self.rsWidget[qApp.focusWidget().id].setEnabled(False)

            
            self.filterBank[qApp.focusWidget().id].update()




    def edtOrderEditingFinished(self, text):
        if hasattr(qApp.focusWidget(), 'id'):
            self.filterBank[qApp.focusWidget().id].order = int(self.edtOrder[qApp.focusWidget().id].text())
            self.filterBank[qApp.focusWidget().id].update()
            
    def lowCritFreqWidgetEditingFinished(self, text):
        if hasattr(qApp.focusWidget(), 'id'):
            if qApp.focusWidget().validator().validate(text, 0)[0] == QValidator.Acceptable:
                self.filterBank[qApp.focusWidget().id].low_crit_freq = float(self.lowCritFreqWidget[qApp.focusWidget().id].text())
                self.filterBank[qApp.focusWidget().id].update()
            elif qApp.focusWidget().validator().validate(text, 0)[0] == QValidator.Invalid:    
                errorMsg = QErrorMessage(self)
                errorMsg.showMessage("Invalid input. Must be a floating point number between 0.001 and 100.0.")
            
    def highCritFreqWidgetEditingFinished(self, text):
        if hasattr(qApp.focusWidget(), 'id'):
            if qApp.focusWidget().validator().validate(text, 0)[0] == QValidator.Acceptable:            
                self.filterBank[qApp.focusWidget().id].high_crit_freq = float(self.highCritFreqWidget[qApp.focusWidget().id].text())
                self.filterBank[qApp.focusWidget().id].update()
            elif qApp.focusWidget().validator().validate(text, 0)[0] == QValidator.Invalid:    
                errorMsg = QErrorMessage(self)
                errorMsg.showMessage("Invalid input. Must be a floating point number between 0.001 and 100.0.")

                
    def rpWidgetEditingFinished(self, text):
        if hasattr(qApp.focusWidget(), 'id'):
            self.filterBank[qApp.focusWidget().id].rp = float(self.rpWidget[qApp.focusWidget().id].text())
            self.filterBank[qApp.focusWidget().id].update()
            
    def rsWidgetEditingFinished(self, text):
        if hasattr(qApp.focusWidget(), 'id'):
            self.filterBank[qApp.focusWidget().id].rs = float(self.rsWidget[qApp.focusWidget().id].text())
            self.filterBank[qApp.focusWidget().id].update()
            
            
            
            
            
    def ckbBoxStateChanged(self, state):
        if hasattr(qApp.focusWidget(), 'id'):
            self.filterBank[qApp.focusWidget().id].useFiltFilt = state == Qt.Checked
            self.filterBank[qApp.focusWidget().id].update()





    def showBtnClicked(self, button):
        print self.filterBank[button.id].low_crit_freq, self.filterBank[button.id].high_crit_freq
        w, AS, AN, Phase = self.filterBank[button.id].computeFilterResponse()
        dlg = filterResponseDlg(w, AS, AN, Phase, self)
        dlg.exec_()
        


    def removeFilter(self, button):
        self.addButton.setFocus()
        self.filterBank.pop(button.id)
        self.updateFilterDisplay()
        

    def updateFilterDisplay(self):
        for i in range(self.centralWidget.layout().rowCount()):
            for i2 in range(self.centralWidget.layout().columnCount()):
                item = self.centralWidget.layout().itemAtPosition(i, i2)
                if not item is None:                
                    self.centralWidget.layout().removeItem(item)
                    wid = item.widget()
                    wid.setParent(None)

        self.mainLayout.removeWidget(self.centralWidget)
        
        self.createBoxLayout()
        self.mainLayout.addWidget(self.centralWidget, 1, 1)     




       


    def addAFilter(self):
        aFilter = Filter(self.sigSignalRates["EEG"])
        aFilter.create(channelType["EEG"], 0.5, 30.0, order=4, btype="bandpass", ftype="butter")        
        self.filterBank.append(aFilter)
        self.updateFilterDisplay()            





import PyQt4.Qwt5 as Qwt

class filterResponseDlg(QDialog):
    def __init__(self, w, AS, AN, Phase, parent=None):
        QDialog.__init__(self, parent)
        self.setWindowTitle('Empirical filter requency response')
        self.box = QGridLayout()
        self.setLayout(self.box)  
        self.MainWin = parent
        self.resize(500,500)



        self.plotter = Qwt.QwtPlot(self)
        self.box.addWidget(self.plotter, 1, 1) 
        self.plotter.setTitle("Frequency response (power)")
        self.plotter.setCanvasBackground(Qt.white)
                
        
        self.phasePlotter = Qwt.QwtPlot(self)
        self.box.addWidget(self.phasePlotter, 2, 1) 
        self.phasePlotter.setTitle("Frequency response (phase)")        
        self.phasePlotter.setCanvasBackground(Qt.white)

        curve = Qwt.QwtPlotCurve("freq. response")    
        #linePen = QPen()
        #linePen.setColor(QColor.black)
        #curve.setPen(linePen)                
        curve.attach(self.plotter)
        SNR = 20*log10(AS/AN)

        curve.setData(w[1:-1], SNR[1:-1]) # On exclut la fréquence nulle puisqu'elle 
                                        # pose problème sur échelle logarithmique       

        #self.plotter.setAxisOptions(Qwt.QwtPlot.xBottom, Qwt.QwtAutoScale.Logarithmic);
        self.plotter.setAxisScaleEngine(Qwt.QwtPlot.xBottom, Qwt.QwtLog10ScaleEngine())
        #self.plotter.setAxisScaleEngine(Qwt.QwtPlot.yLeft, Qwt.QwtLog10ScaleEngine())
            
        #print w
        #self.plotter.setAxisScale(Qwt.QwtPlot.xBottom, min(w[1:-1]), max(w[1:-1]))             
            
            


        phaseCurve = Qwt.QwtPlotCurve("freq. response (phase)")    
        #linePen = QPen()
        #linePen.setColor(QColor.black)
        #curve.setPen(linePen)                
        phaseCurve.attach(self.phasePlotter)

        phaseCurve.setData(w[1:-1], Phase[1:-1]) # On exclut la fréquence nulle puisqu'elle 
                                        # pose problème sur échelle logarithmique       

        self.phasePlotter.setAxisScaleEngine(Qwt.QwtPlot.xBottom, Qwt.QwtLog10ScaleEngine())
          
                        
            
            
            

class setSignalsDlg(QDialog):
    def __init__(self, parent=None):
        QDialog.__init__(self, parent)
        self.setWindowTitle('Set signal dialog')
        self.resize(100,100)

        self.box = QGridLayout()
        self.setLayout(self.box)  
        self.MainWin = parent

    def setReader(self, reader=None):
        self.reader = reader
        
        if self.reader is None:
            errMessage = QErrorMessage(self)            
            errMessage.showMessage(QString("Error: " + "You first need to open a data file."))
            return


        i = 0
        self.box.addWidget(QLabel('signal'), 1, 1)
        self.box.addWidget(QLabel('display'), 1, 2)                
        self.box.addWidget(QLabel('sensitivity (uV/mm)'), 1, 3)                
        self.box.addWidget(QLabel('space'), 1, 4)                           
        self.box.addWidget(QLabel('color'), 1, 5)                
        self.buttonGroupe = QButtonGroup(self) 
        
        self.buttonGroupe.buttonClicked.connect(self.colorBtnClicked)
                
        if os.path.exists("SigDisplayInfo.pck"):        
            pckSigDisplayInf = cPickle.load(file("SigDisplayInfo.pck", "r"))               
        else:
            pckSigDisplayInf = SigDisplayInfo()
                
        for label, i in zip(self.reader.getChannelLabels(), range(len(self.reader.getChannelLabels()))) :
            self.box.addWidget(QLabel(label), i+2, 1)
            ckbBox = QCheckBox()            
            self.box.addWidget(ckbBox, i+2, 2)    
            pckItem = pckSigDisplayInf.getSignal(label) 
            button  = QPushButton()
            button.id = i
            self.box.addWidget(button, i+2, 5)
            self.buttonGroupe.addButton(button, i)
            
            if len(pckItem) == 1 : # Si on a en mémoire une configuration pour ce canal           
                ckbBox.setChecked(True)
                self.box.addWidget(QLineEdit(str(pckItem[0].sensitivity)), i+2, 3)
                self.box.addWidget(QLineEdit(str(pckItem[0].space)), i+2, 4) 
                button.setStyleSheet("* { background-color: rgb(" + str(pckItem[0].sigColor.red()) + "," 
                            + str(pckItem[0].sigColor.green()) + "," + str(pckItem[0].sigColor.blue()) + ") }")
        
            else:  # Sinon, on utilise des valeurs par défauts          
                ckbBox.setChecked(False)
                self.box.addWidget(QLineEdit("7.5"), i+2, 3)
                self.box.addWidget(QLineEdit("1"), i+2, 4)
                button.setStyleSheet("* { background-color: rgb(0,0,0) }")
                
            
        # À corriger pour faire apparaitre les boutons...!
        buttonBox = QDialogButtonBox(QDialogButtonBox.Ok
                                      | QDialogButtonBox.Cancel)         
        
        buttonBox.accepted.connect(self.accept );  
        buttonBox.rejected.connect(self.reject );        
        
        self.box.addWidget(buttonBox, i+3, 1)



    def colorBtnClicked(self, button):
        palette = button.palette()

        newButtonColor = QColorDialog.getColor(button.palette().color(QPalette.Button), self)
        #palette.setColor(QPalette.Button, newButtonColor)
        #button.setPalette(palette)
        #button.repaint()
        #self.repaint()
        button.setStyleSheet("* { background-color: rgb(" + str(newButtonColor.red()) + "," 
                            + str(newButtonColor.green()) + "," + str(newButtonColor.blue()) + ") }")


    def accept(self):
        self.MainWin.SigDisplayInf.clear()
                   
        for i in range(len(self.reader.getChannelLabels())) :
            if self.box.itemAtPosition (i+2, 2).widget().isChecked():              
                name        =  self.box.itemAtPosition (i+2, 1).widget().text()
                sensitivity = float(self.box.itemAtPosition (i+2, 3).widget().text())
                space       = int(self.box.itemAtPosition (i+2, 4).widget().text())
                color       = self.box.itemAtPosition (i+2, 5).widget().palette().color(QPalette.Button)
                self.MainWin.SigDisplayInf.addSignal(name, sensitivity, space, color)
        
        #MainWindow.updateSignals(self.MainWin)        
        
        cPickle.dump(self.MainWin.SigDisplayInf, file("SigDisplayInfo.pck", "w"))        
        
        QDialog.accept(self)

    def reject(self):
        self.MainWin.SigDisplayInf.clear()
        QDialog.reject(self)
        
        