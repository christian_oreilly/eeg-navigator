# -*- coding: utf-8 -*-
"""
Created on Tue Aug 27 14:09:43 2013

@author: oreichri
"""
from PyQt4.QtCore import *
from PyQt4.QtGui import *


    
class Record:
    def __init__(self, name, sensitivity, space, sigColor = QColor("black")):        
        self.sensitivity    = sensitivity
        self.space          = space
        self.name           = name    
        self.sigColor       = sigColor



class SigDisplayInfo:
    def __init__(self):
        self.clear()
        
    def clear(self):
        self.nbSignals      = 0
        self.records        = []

    def addSignal(self, name, sensitivity, space, color = QColor("black")):
        self.nbSignals += 1
        self.records.append(Record(name, sensitivity, space, color))


    def getSignal(self, name):
        return filter(lambda e: e.name == name, self.records)
