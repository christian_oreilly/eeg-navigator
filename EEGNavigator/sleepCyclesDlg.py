# -*- coding: utf-8 -*-
"""
Created on Wed Jun 27 19:39:43 2012

@author: REVESTECH
"""


from PyQt4.QtCore import *
from PyQt4.QtGui import *

import cPickle

from copy import deepcopy

    
    
    
class changeCycleDefinitionDlg(QDialog):
    def __init__(self, parent, sleepCycleDef):
        QDialog.__init__(self, parent)

        self.sleepCycleDef = sleepCycleDef
        
        # Predefine option group
        predfineLayout = QGridLayout()

        self.feinbergBtn    = QPushButton("Feinberg & Floyd 1979")
        self.schulzBtn      = QPushButton("Schulz 1980")
        self.aeschbachBtn   = QPushButton("Aeschbach 1993")
        self.minimalBtn     = QPushButton("Minimum criteria")

        self.feinbergBtn.clicked.connect(self.chooseFeinberg)           
        self.schulzBtn.clicked.connect(self.chooseSchulz)           
        self.aeschbachBtn.clicked.connect(self.chooseAeschbach)           
        self.minimalBtn.clicked.connect(self.chooseMinimal)           



        predfineLayout.addWidget(self.feinbergBtn, 1, 1)
        predfineLayout.addWidget(self.schulzBtn, 1, 2)
        predfineLayout.addWidget(self.aeschbachBtn, 2, 1)
        predfineLayout.addWidget(self.minimalBtn, 2, 2)

        predefineFrame = QGroupBox("Pre-defined options")
        predefineFrame.setLayout(predfineLayout)


        # Stade group
        stadeLayout = QGridLayout()

        #self.comboFallingAsleep    = QComboBox()
        #self.comboSleeping         = QComboBox()
        #stadeLayout.addWidget(QLabel("Falling asleep determining stage"), 1, 1)
        #stadeLayout.addWidget(self.comboFallingAsleep, 1, 2)
        #stadeLayout.addWidget(QLabel("Sleep stages (after having fall asleep)"), 2, 1)
        #stadeLayout.addWidget(self.comboSleeping, 2, 2)


        self.asleep1minDurationEdt  = QLineEdit("0")
        self.asleep1minDurationEdt.textEdited.connect(self.lineEditChanged)  
        self.asleep1Chk             = QCheckBox("Stage1")
        self.asleep1Chk.stateChanged.connect(self.checkBoxStateChanged)           
        self.asleep2Chk             = QCheckBox("Stage2")
        self.asleep2Chk.stateChanged.connect(self.checkBoxStateChanged)           
        self.asleep3Chk             = QCheckBox("Stage3")
        self.asleep3Chk.stateChanged.connect(self.checkBoxStateChanged)           
        self.asleep4Chk             = QCheckBox("Stage4")
        self.asleep4Chk.stateChanged.connect(self.checkBoxStateChanged)           
        self.asleepREMChk           = QCheckBox("REM")
        self.asleepREMChk.stateChanged.connect(self.checkBoxStateChanged)           


        self.sleep1Chk             = QCheckBox("Stage1")
        self.sleep1Chk.stateChanged.connect(self.checkBoxStateChanged)           
        self.sleep2Chk             = QCheckBox("Stage2")
        self.sleep2Chk.stateChanged.connect(self.checkBoxStateChanged)     
        self.sleep3Chk             = QCheckBox("Stage3")
        self.sleep3Chk.stateChanged.connect(self.checkBoxStateChanged)     
        self.sleep4Chk             = QCheckBox("Stage4")
        self.sleep4Chk.stateChanged.connect(self.checkBoxStateChanged)     
        self.sleepREMChk           = QCheckBox("REM")
        self.sleepREMChk.stateChanged.connect(self.checkBoxStateChanged)         
        self.layoutSleeping         = QHBoxLayout()
     
        
        
        
        stadeLayout.addWidget(QLabel("Falling asleep determining stage : "), 1, 1)
        stadeLayout.addWidget(self.asleep1minDurationEdt, 1, 2) 
        stadeLayout.addWidget(QLabel("min of stage"), 1, 3) 
        stadeLayout.addWidget(self.asleep1Chk, 1, 4) 
        stadeLayout.addWidget(self.asleep2Chk, 1, 5) 
        stadeLayout.addWidget(self.asleep3Chk, 1, 6) 
        stadeLayout.addWidget(self.asleep4Chk, 1, 7) 
        stadeLayout.addWidget(self.asleepREMChk, 1, 8) 
            
        
        
        stadeLayout.addWidget(QLabel("Sleep stages (after having fall asleep) : "), 2, 1)
        stadeLayout.addWidget(self.sleep1Chk, 2, 4) 
        stadeLayout.addWidget(self.sleep2Chk, 2, 5) 
        stadeLayout.addWidget(self.sleep3Chk, 2, 6) 
        stadeLayout.addWidget(self.sleep4Chk, 2, 7) 
        stadeLayout.addWidget(self.sleepREMChk, 2, 8)    

        stadeFrame = QGroupBox("Stages")
        stadeFrame.setLayout(stadeLayout)





        # NREM period group
        nremLayout = QGridLayout()

        self.minTimeNREMEdt        = QLineEdit()
        self.minTimeNREMEdt.textEdited.connect(self.lineEditChanged)  
        self.minTimeLastNREMEdt    = QLineEdit()
        self.minTimeLastNREMEdt.textEdited.connect(self.lineEditChanged)  


        nremLayout.addWidget(QLabel("Minimal duration of a NREM period : "), 1, 1)
        nremLayout.addWidget(self.minTimeNREMEdt, 1, 2)
        nremLayout.addWidget(QLabel("min."), 1, 3)
        nremLayout.addWidget(QLabel("Minimal duration of the last NREM period : "), 2, 1)
        nremLayout.addWidget(self.minTimeLastNREMEdt, 2, 2)
        nremLayout.addWidget(QLabel("min."), 2, 3)

        nremFrame = QGroupBox("NREM periods")
        nremFrame.setLayout(nremLayout)
        
        




        # REM period group
        remLayout = QGridLayout()

        self.minTimeREMExceptFirstEdt   = QLineEdit()
        self.minTimeREMExceptFirstEdt.textEdited.connect(self.lineEditChanged)  
        self.minTimeLastREMEdt          = QLineEdit()
        self.minTimeLastREMEdt.textEdited.connect(self.lineEditChanged)  
        self.maxPeriodWithoutREMEdt     = QLineEdit()
        self.maxPeriodWithoutREMEdt.textEdited.connect(self.lineEditChanged)  
        self.cylesEndWithNextNREMChk    = QCheckBox("The cycle ends with the next NREM period")
        self.cylesEndWithNextNREMChk.stateChanged.connect(self.checkBoxStateChanged)   


        remLayout.addWidget(QLabel("Minimal duration of a REM period (except the first one) : "), 1, 1)
        remLayout.addWidget(self.minTimeREMExceptFirstEdt, 1, 2)
        remLayout.addWidget(QLabel("min."), 1, 3)
        remLayout.addWidget(QLabel("Minimal duration of the last REM period : "), 2, 1)
        remLayout.addWidget(self.minTimeLastREMEdt, 2, 2)
        remLayout.addWidget(QLabel("min."), 2, 3)
        remLayout.addWidget(QLabel("Maximal period without REM : "), 3, 1)
        remLayout.addWidget(self.maxPeriodWithoutREMEdt, 3, 2)
        remLayout.addWidget(QLabel("min."), 3, 3)
        remLayout.addWidget(self.cylesEndWithNextNREMChk, 4, 1, 1, 3)


        remFrame = QGroupBox("REM periods")
        remFrame.setLayout(remLayout)
        
        
        

        # Control buttons
        buttonBox = QDialogButtonBox(QDialogButtonBox.Ok
                                      | QDialogButtonBox.Cancel)         
        
        buttonBox.accepted.connect(self.accept );  
        buttonBox.rejected.connect(self.reject );        

        
        typeCycleLayout = QHBoxLayout()
        self.typeCycleDefEdt = QLineEdit()
        typeCycleLayout.addWidget(QLabel("Type of cycle definition: "))
        typeCycleLayout.addWidget(self.typeCycleDefEdt)

        dlgLayout = QGridLayout()
        dlgLayout.addWidget(predefineFrame, 1, 1)
        dlgLayout.addWidget(stadeFrame, 1, 2)
        dlgLayout.addWidget(nremFrame, 2, 1)
        dlgLayout.addWidget(remFrame, 2, 2)
        dlgLayout.addLayout(typeCycleLayout, 3, 1)
        dlgLayout.addWidget(buttonBox, 3, 2)
  
  
        self.setLayout(dlgLayout)

        self.setCycleDefinition(sleepCycleDef)



    def lineEditChanged(self, text):  
        self.typeCycleDefEdt.setText("custom definition")   
        
    def checkBoxStateChanged(self, state):  
        self.typeCycleDefEdt.setText("custom definition") 


    def chooseFeinberg(self):   
        cycleDef = cycleDefinitions()
        cycleDef.setFeinberg()        
        self.setCycleDefinition(cycleDef)
            
            
    def chooseSchulz(self):   
        cycleDef = cycleDefinitions()
        cycleDef.setSchulz()        
        self.setCycleDefinition(cycleDef)            
            
            
    def chooseAeschbach(self):   
        cycleDef = cycleDefinitions()
        cycleDef.setAeschbach()        
        self.setCycleDefinition(cycleDef)
                    
            
    def chooseMinimal(self):   
        cycleDef = cycleDefinitions()
        cycleDef.setMinimal()        
        self.setCycleDefinition(cycleDef)


    def setCycleDefinition(self, cycleDef):

        self.asleep1minDurationEdt.setText(str(cycleDef.minStage1ForSleep ))
        
        self.asleep1Chk.setChecked("Stage1" in cycleDef.sleepDeterminingStages)
        self.asleep2Chk.setChecked("Stage2" in cycleDef.sleepDeterminingStages)
        self.asleep3Chk.setChecked("Stage3" in cycleDef.sleepDeterminingStages)
        self.asleep4Chk.setChecked("Stage4" in cycleDef.sleepDeterminingStages)
        self.asleepREMChk.setChecked("REM" in cycleDef.sleepDeterminingStages)


        self.sleep1Chk.setChecked("Stage1" in cycleDef.sleepStages)
        self.sleep2Chk.setChecked("Stage2" in cycleDef.sleepStages)
        self.sleep3Chk.setChecked("Stage3" in cycleDef.sleepStages)
        self.sleep4Chk.setChecked("Stage4" in cycleDef.sleepStages)
        self.sleepREMChk.setChecked("REM" in cycleDef.sleepStages)   
  
        self.minTimeNREMEdt.setText(str(cycleDef.minTimeNREM ))
        self.minTimeLastNREMEdt.setText(str(cycleDef.minTimeLastNREM ))

        self.minTimeREMExceptFirstEdt.setText(str(cycleDef.minTimeREMExceptFirst ))
        self.minTimeLastREMEdt.setText(str(cycleDef.minTimeLastREM ))
        self.maxPeriodWithoutREMEdt.setText(str(cycleDef.maxPeriodWithoutREM ))
        self.cylesEndWithNextNREMChk.setChecked(cycleDef.cylesEndWithNextNREM )
        
        self.typeCycleDefEdt.setText(cycleDef.type)


    def accept(self):        
        self.sleepCycleDef.minStage1ForSleep = float(self.asleep1minDurationEdt.text())
        
        sleepDeterminingStages = []
        if self.asleep1Chk.isChecked():
            sleepDeterminingStages.append("Stage1")
        if self.asleep2Chk.isChecked():
            sleepDeterminingStages.append("Stage2")
        if self.asleep3Chk.isChecked():
            sleepDeterminingStages.append("Stage3")
        if self.asleep4Chk.isChecked():
            sleepDeterminingStages.append("Stage4")
        if self.asleepREMChk.isChecked():
            sleepDeterminingStages.append("REM")
        self.sleepCycleDef.sleepDeterminingStages = sleepDeterminingStages       
        
        sleepStages = []
        if self.sleep1Chk.isChecked():
            sleepStages.append("Stage1")
        if self.sleep2Chk.isChecked():
            sleepStages.append("Stage2")
        if self.sleep3Chk.isChecked():
            sleepStages.append("Stage3")
        if self.sleep4Chk.isChecked():
            sleepStages.append("Stage4")
        if self.sleepREMChk.isChecked():
            sleepStages.append("REM")
        self.sleepCycleDef.sleepStages = sleepStages     
 
  
        self.sleepCycleDef.minTimeNREM      = float(self.minTimeNREMEdt.text())
        self.sleepCycleDef.minTimeLastNREM  = float(self.minTimeLastNREMEdt.text())

        self.sleepCycleDef.minTimeREMExceptFirst    = float(self.minTimeREMExceptFirstEdt.text())
        self.sleepCycleDef.minTimeLastREM           = float(self.minTimeLastREMEdt.text())
        self.sleepCycleDef.maxPeriodWithoutREM      = float(self.maxPeriodWithoutREMEdt.text())
        self.sleepCycleDef.cylesEndWithNextNREM     = self.cylesEndWithNextNREMChk.isChecked()


        self.sleepCycleDef.type = str(self.typeCycleDefEdt.text())

        cPickle.dump(self.sleepCycleDef, file("sleepCycleDefClass.pck", "w"))       
        QDialog.accept(self)


    def reject(self):
        QDialog.reject(self)




