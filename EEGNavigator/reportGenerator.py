# -*- coding: utf-8 -*-
"""
Created on Fri Jun 29 13:21:54 2012

@author: REVESTECH
"""

from PolyConst import stageDisplayName
from sleepCyclesDlg import changeCycleDefinitionDlg


from spyndle.miscellaneous import computeDreamCycles        

from scipy import array, mean, std   
from copy import deepcopy


#def relativeTimeToAbsoluteTime(relativeTime):
#    
 #   import datetime
#
 #   hour  = int(relativeTime/3600.0)%24           
  #  minute = int((relativeTime - int(relativeTime/3600.0)*3600.0)/60.0)%60   
   # seconde = int(relativeTime - int(relativeTime/3600.0)*3600.0 - minute*60.0)       
    #
    #return datetime.time(hour, minute, seconde)      

class SpindleReportGenerator:
        
    def generate(self, fileName, MainWin, montage, channel, eventName):
        f = file(fileName, "w")        
        f.write('<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">\n')
        f.write('<html lang="fr-ca">\n')
        f.write('<head>\n')
        f.write('   <title>Rapport DetecteFuseaux</title>\n')
        f.write('   	<meta content="text/html;charset=ISO-8859-1" http-equiv="Content-Type">\n')
        f.write('   	<meta name="generator" content="EDFViewer v0.1">\n')
        f.write('   	<meta name="author" content="Christian O\'Reilly (format calqué sur les rapports de Ga&eacute;tan Poirier)">\n')
        f.write('   	<meta name="description" content="Rapport de d&eacute;tection automatique des fuseaux">\n')
        f.write('   \n')
        f.write('   	<style type="text/css">\n')
        f.write('   	body\n')
        f.write('   	{	font-size:small;\n')
        f.write('   	}\n')
        f.write('   	th\n')
        f.write('   	{	font-style:italic;\n')
        f.write('   	}\n')
        f.write('   	</style>\n')
        f.write('   </head>\n')
        f.write('   <body>\n')
        f.write('   \n')
        f.write('   <big><b>DetecteFuseaux - Version 3.53 : Rapport de d&eacute;tection automatique des fuseaux</b></big><br>\n')
        f.write('   <b>Nom de l\'&eacute;v&eacute;nement : </b>' + eventName + '<BR>\n')
        
        
        # Identification
        f.write('   <hr>\n')
        f.write('   <big><b>Identification</b></big>\n')
        f.write('   <br><br>\n')
        f.write('   <table border="0" cellpadding="0" cellspacing="0"><tbody>\n')
        f.write('       <tr><th style="text-align: left; width: 92px;">Fichier :</th><td style="text-align: left;">' + MainWin.fileName + '</td></tr>\n')
        f.write('       <tr><th style="text-align: left;">Nom :</th><td style="text-align: left;">' +         MainWin.reader.patientInfo["firstname"] + ', ' +  MainWin.reader.patientInfo["lastname"] + '</td></tr>\n')
        f.write('       <tr><th style="text-align: left;">Identifications :</th><td style="text-align: left;">' +         MainWin.reader.patientInfo["Id1"] + '</td></tr>\n')
        f.write('       <tr><th style="text-align: left;">Date :</th><td style="text-align: left;">' + str(MainWin.reader.recordingStartDateTime) + '</td></tr>\n')
        f.write('       <tr><th style="text-align: left;">Montage :</th><td style="text-align: left;">' + montage + '</td></tr>\n')
        f.write('       <tr><th style="text-align: left;">Canal :</th><td style="text-align: left;">' + channel + ' (EEG, 128 Hz)</td></tr>\n')
        f.write('   </tbody></table>\n')
        
        
        # Découpage en cycles
        fallingInSleepString = ""          
        for stage in  MainWin.sleepCycleDef.sleepDeterminingStages:
            if stage == "Stage1" and MainWin.sleepCycleDef.minStage1ForSleep :
                fallingInSleepString += str(MainWin.sleepCycleDef.minStage1ForSleep) + " min de stade 1, "
            else:
                fallingInSleepString += stageDisplayName[stage] + ", "                
        fallingInSleepString = fallingInSleepString[0:-2]
                    
        sleepingString = ""          
        for stage in  MainWin.sleepCycleDef.sleepStages:
            sleepingString += stageDisplayName[stage] + ", "                
        sleepingString = sleepingString[0:-2]
            
        f.write('   <hr>\n')
        f.write('   <big><b>Options de d&eacute;coupage des cycles de sommeil</b></big>\n')
        f.write('   <br><br>\n')
        f.write('   <table border="0" cellpadding="0" cellspacing="0"><tbody>\n')
        f.write('       <tr><th style="text-align: left; width: 356px;">Stades d&eacute;terminant l\'endormissement :</th><td style="text-align: right;">'  + fallingInSleepString +  '</td><td></td></tr>\n')
        f.write('       <tr><th style="text-align: left;">Stades de sommeil (apr&egrave;s l\'endormissement) :</th><td style="text-align: right;">' + sleepingString + '</td><td></td></tr>\n')
        f.write('       <tr><th style="text-align: left;">Longueur minimum d\'une p&eacute;riode NREM :</th><td style="text-align: right;">' + str(MainWin.sleepCycleDef.minTimeNREM) + '</td><td style="text-align: right;">&nbsp;minutes</td></tr>\n')
        f.write('       <tr><th style="text-align: left;">Longueur minimum de la derni&egrave;re p&eacute;riode NREM :</th><td style="text-align: right;">' + str(MainWin.sleepCycleDef.minTimeLastNREM) + '</td><td style="text-align: right;">&nbsp;minutes</td></tr>\n')
        f.write('       <tr><th style="text-align: left;">Longueur minimum d\'une p&eacute;riode REM sauf la premi&egrave;re :</th><td style="text-align: right;">' + str(MainWin.sleepCycleDef.minTimeREMExceptFirst) + '</td><td style="text-align: right;">&nbsp;minutes</td></tr>\n')
        f.write('       <tr><th style="text-align: left;">Longueur minimum de la derni&egrave;re p&eacute;riode REM :</th><td style="text-align: right;">' + str(MainWin.sleepCycleDef.minTimeLastREM) + '</td><td style="text-align: right;">&nbsp;minutes</td></tr>\n')
        f.write('       <tr><th style="text-align: left;">Fin d\'une p&eacute;riode REM apr&egrave;s :</th><td style="text-align: right;">' + str(MainWin.sleepCycleDef.maxPeriodWithoutREM) + '</td><td style="text-align: right;">&nbsp;minutes</td></tr>\n')
        f.write('       <tr><th style="text-align: left;">Fin de la p&eacute;riode REM au d&eacute;but de la p&eacute;riode NREM suivante :</th><td style="text-align: right;">' + str(MainWin.sleepCycleDef.cylesEndWithNextNREM) + '</td><td></td></tr>\n')
        f.write('       <tr><th style="text-align: left;">Type de d&eacute;coupage :</th><td style="text-align: right;">' + MainWin.sleepCycleDef.type + '</td><td></td></tr>\n')
        f.write('       <tr><th style="text-align: left;">Inclure les cycles incomplets :</th><td style="text-align: right;">' + '##########'  + '</td><td></td></tr>\n')
        f.write('   </tbody></table>\n')
        
        
        
        # Option de détection
        f.write('   <hr>\n')
        f.write('   <big><b>Options de d&eacute;tection</b></big>\n')
        f.write('   <br><br>\n')
        
        f.write('   Fuseaux pr&eacute;d&eacute;tect&eacute;s\n')
        f.write('   <br><br>\n')
        
#        f.write('   <table border="0" cellpadding="0" cellspacing="0"><tbody>\n')
#        f.write('       <tr><th style="text-align: left; width: 249px;">Filtre :</th><td style="text-align: left;">passe-bande de 11.1 &agrave; 14.9Hz, ordre 511, fen&ecirc;tre rectangulaire, normalis&eacute;</td></tr>\n')
#        f.write('       <tr><th style="text-align: left;">Longueur de la fen&ecirc;tre RMS :</th><td style="text-align: left;">0.25 secondes</td></tr>\n')
#        f.write('       <tr><th style="text-align: left;">Seuil de d&eacute;tection des fuseaux :</th><td style="text-align: left;">95e percentile de l\'amplitude RMS</td></tr>\n')
#        f.write('       <tr><th style="text-align: left;">Dur&eacute;e des fuseaux :</th><td style="text-align: left;">de 0.5 &agrave; 20 secondes</td></tr>\n')
#        f.write('       <tr><th style="text-align: left;">Exclure les p&eacute;riode REM :</th><td style="text-align: left;">Oui</td></tr>\n')
#        f.write('       <tr><th style="text-align: left;">Calcul  du seuil pour chaque cycle :</th><td style="text-align: left;">Oui</td></tr>\n')
#        f.write('       <tr><th style="text-align: left;">D&eacute;tecter les &eacute;v&eacute;nements pendant les stades<sup>1</sup> :</th><td style="text-align: left;">2, 3, 4</td></tr>\n')
#        f.write('       <tr><th style="text-align: left;">Ne pas d&eacute;tecter lors de :</th><td style="text-align: left;">(Aucun)</td></tr>\n')
#        f.write('   </tbody></table>\n')
#        f.write('   <hr>\n')
#        f.write('   <big><b>Avertissements lors du d&eacute;coupage des cycles</b></big>\n')
#        f.write('   <br><br>\n')
#        f.write('   &nbsp;1 : Une courte p&eacute;riode REM (< 2 min) a &eacute;t&eacute; observ&eacute;e.<br>\n')




        # Résultats
        stageEventLst = filter(lambda e: e.groupName == "Stage", MainWin.reader.events)   
        self.dreamCycles = computeDreamCycles(stageEventLst, MainWin.sleepCycleDef)           

        # Total
        duration  = {}
        for state in ["WAKE", "Stage1", "Stage2", "Stage3", "Stage4", "REM", "StageU"]:  
            duration[state] = 0.0
        
        # par cycle
        durations = []        
        for cycle in self.dreamCycles:
            durations.append(deepcopy(duration))

            
        for cycle, i in zip(self.dreamCycles, range(len(self.dreamCycles))):
            for state in ["WAKE", "Stage1", "Stage2", "Stage3", "Stage4", "REM", "StageU"]:  
                REMdur  = sum(array([stage.timeLength for stage in cycle.REMpages if stage.name == state]))/60.0
                NREMdur = sum(array([stage.timeLength for stage in cycle.NREMpages if stage.name == state]))/60.0
                duration[state] += REMdur + NREMdur
                durations[i][state] = REMdur + NREMdur
            
        totalDuration = 0.0
        for state in ["WAKE", "Stage1", "Stage2", "Stage3", "Stage4", "REM", "StageU"]:  
            totalDuration += duration[state]           

        totalDurations = []
        for cycle, i in zip(self.dreamCycles, range(len(self.dreamCycles))):
            temp = 0.0
            for state in ["WAKE", "Stage1", "Stage2", "Stage3", "Stage4", "REM", "StageU"]:  
                temp += durations[i][state]        
            totalDurations.append(temp)
                
        #stageEventLst = filter(lambda e: e.groupName == "Stage", MainWin.reader.events)   
        #durationWake = sum(array([stage.timeLength for stage in stageEventLst if stage.name == "WAKE"]))/60.0
        #duration1    = sum(array([stage.timeLength for stage in stageEventLst if stage.name == "Stage1"]))/60.0
        #duration2    = sum(array([stage.timeLength for stage in stageEventLst if stage.name == "Stage2"]))/60.0
        #duration3    = sum(array([stage.timeLength for stage in stageEventLst if stage.name == "Stage3"]))/60.0
        #duration4    = sum(array([stage.timeLength for stage in stageEventLst if stage.name == "Stage4"]))/60.0
        #durationREM  = sum(array([stage.timeLength for stage in stageEventLst if stage.name == "REM"]))/60.0
        #durationIND  = sum(array([stage.timeLength for stage in stageEventLst if stage.name == "StageU"]))/60.0
       
       
        spindleEventLst = filter(lambda e: e.name == eventName and e.channel == channel, MainWin.reader.events)  
        for cycle in self.dreamCycles:
            cycle.Spindles = filter(lambda e: e.startSample >= cycle.sampleStartNREM  and e.startSample  < cycle.sampleStartNREM  + cycle.sampleDurationNREM   + cycle.sampleDurationREM  , spindleEventLst)  
        # Problème potentiel avec l'étiquetage du temps dans les évènements Fuseau  
        # lorsqu'extrait avec les outils de Gaétan. Seulement sur certain fichiers
        # (voir p. ex. ADABON1_h).
        # Code test
        #for spindle in spindleEventLst:        
        #    print relativeTimeToAbsoluteTime(spindle.startTime), spindle.dateTime
        #
        #for spindle in stageEventLst:        
        #    print relativeTimeToAbsoluteTime(spindle.startTime), spindle.dateTime
    
        for spindle in spindleEventLst:
            #stage = filter(lambda stage: stage.startTime <= spindle.startTime and stage.startTime + stage.timeLength >= spindle.startTime, stageEventLst)  
            stage = filter(lambda stage: stage.startSample <= spindle.startSample and stage.startSample + stage.sampleLength >= spindle.startSample, stageEventLst)  
            
            if len(stage) == 1:
                spindle.stageName = stage[0].name
                #print "%0.1f" % stage[0].startTime, "%0.1f" % spindle.startTime, "%0.1f" % (stage[0].startTime + stage[0].timeLength),  "       :         ",  "%0.1f" % stage[0].startSample, "%0.1f" % spindle.startSample, "%0.1f" % (stage[0].startSample + stage[0].sampleLength) 
            
                
            else:
                spindle.stageName = "stageAssociationError"
    
        # Total
        spindleByState  = {}
        secondeByState  = {}
        freqencyByState = {}
        ampRMSByState   = {}
        ampPaPByState   = {}
        for state in ["WAKE", "Stage1", "Stage2", "Stage3", "Stage4", "REM", "StageU"]:        
            spindleByState[state] = [spindle for spindle in spindleEventLst if spindle.stageName == state]
        
            if len(spindleByState[state]):
                spindleDuration  = array([spindle.timeLength for spindle in spindleByState[state]])
                spindleFrequency = array([float(spindle.properties['Fr\xe9quence']) for spindle in spindleByState[state]])
                spindleRMS       = array([float(spindle.properties['Amplitude RMS']) for spindle in spindleByState[state]])
                spindlePaP       = array([float(spindle.properties['Amplitude pic \xe0 pic']) for spindle in spindleByState[state]])
                secondeByState[state]  = '%0.1f&plusmn;%0.1f' % (mean(spindleDuration), std(spindleDuration))
                freqencyByState[state] = '%0.1f&plusmn;%0.1f' % (mean(spindleFrequency), std(spindleFrequency))
                ampRMSByState[state]   = '%0.1f&plusmn;%0.1f' % (mean(spindleRMS), std(spindleRMS))
                ampPaPByState[state]   = '%0.1f&plusmn;%0.1f' % (mean(spindlePaP), std(spindlePaP))            
            else:
                secondeByState[state] = '---'    
                freqencyByState[state] = '---'   
                ampRMSByState[state] = '---'   
                ampPaPByState[state] = '---'   

        if len(spindleEventLst):
            spindleDuration = array([spindle.timeLength for spindle in spindleEventLst])            
            totalSecond     = '%0.1f&plusmn;%0.1f' % (mean(spindleDuration), std(spindleDuration))
            totalFrequency  = '%0.1f&plusmn;%0.1f' % (mean(spindleFrequency), std(spindleFrequency))
            totalAmpRMS     = '%0.1f&plusmn;%0.1f' % (mean(spindleRMS), std(spindleRMS))
            totalAmpPap     = '%0.1f&plusmn;%0.1f' % (mean(spindlePaP), std(spindlePaP))      
        else:
            totalSecond    = '---'
            totalFrequency = '---'
            totalAmpRMS    = '---'
            totalAmpPap    = '---'

        # par cycke
        spindleByStates  = []
        secondeByStates  = []       
        freqencyByStates = []
        ampRMSByStates   = []
        ampPaPByStates   = []        
        totalSeconds     = []        
        totalFrequencies = []
        totalAmpRMSs     = [] 
        totalAmpPaps     = []
        for cycle in self.dreamCycles:
            spindleByStateTmp  = {}
            secondeByStateTmp  = {}   
            freqencyByStateTmp = {}
            ampRMSByStateTmp   = {}
            ampPaPByStateTmp   = {}            
            for state in ["WAKE", "Stage1", "Stage2", "Stage3", "Stage4", "REM", "StageU"]:        
                spindleByStateTmp[state] = [spindle for spindle in cycle.Spindles if spindle.stageName == state]
            
                if len(spindleByStateTmp[state]):
                    #spindleDuration = array([spindle.timeLength for spindle in spindleByStateTmp[state]])
                    #secondeByStateTmp[state] = '%0.1f&plusmn;%0.1f' % (mean(spindleDuration), std(spindleDuration))
                    spindleDuration  = array([spindle.timeLength for spindle in spindleByState[state]])
                    spindleFrequency = array([float(spindle.properties['Fr\xe9quence']) for spindle in spindleByState[state]])
                    spindleRMS       = array([float(spindle.properties['Amplitude RMS']) for spindle in spindleByState[state]])
                    spindlePaP       = array([float(spindle.properties['Amplitude pic \xe0 pic']) for spindle in spindleByState[state]])
                    secondeByStateTmp[state]  = '%0.1f&plusmn;%0.1f' % (mean(spindleDuration), std(spindleDuration))
                    freqencyByStateTmp[state] = '%0.1f&plusmn;%0.1f' % (mean(spindleFrequency), std(spindleFrequency))
                    ampRMSByStateTmp[state]   = '%0.1f&plusmn;%0.1f' % (mean(spindleRMS), std(spindleRMS))
                    ampPaPByStateTmp[state]   = '%0.1f&plusmn;%0.1f' % (mean(spindlePaP), std(spindlePaP))                       
                        
                    
                else:
                    secondeByStateTmp[state]  = '---'  
                    freqencyByStateTmp[state] = '---'
                    ampRMSByStateTmp[state]   = '---'
                    ampPaPByStateTmp[state]   = '---'                    
    
            if len(spindleEventLst):
                spindleDuration = array([spindle.timeLength for spindle in cycle.Spindles])       
                spindleFrequency = array([float(spindle.properties['Fr\xe9quence']) for spindle in cycle.Spindles])
                spindleRMS       = array([float(spindle.properties['Amplitude RMS']) for spindle in cycle.Spindles])
                spindlePaP       = array([float(spindle.properties['Amplitude pic \xe0 pic']) for spindle in cycle.Spindles])

                totalSecondTmp      = '%0.1f&plusmn;%0.1f' % (mean(spindleDuration), std(spindleDuration))
                totalFrequencyTmp   = '%0.1f&plusmn;%0.1f' % (mean(spindleFrequency), std(spindleFrequency))
                totalAmpRMSTmp      = '%0.1f&plusmn;%0.1f' % (mean(spindleRMS), std(spindleRMS))
                totalAmpPapTmp      = '%0.1f&plusmn;%0.1f' % (mean(spindlePaP), std(spindlePaP)) 
            else:
                totalSecondTmp      = '---'
                totalFrequencyTmp   = '---'
                totalAmpRMSTmp      = '---'
                totalAmpPapTmp      = '---'                

            spindleByStates.append(spindleByStateTmp)
            secondeByStates.append(secondeByStateTmp)
            freqencyByStates.append(freqencyByStateTmp)
            ampRMSByStates.append(ampRMSByStateTmp)
            ampPaPByStates.append(ampPaPByStateTmp)           
                  
            totalSeconds.append(totalSecondTmp)           
            totalFrequencies.append(totalFrequencyTmp)    
            totalAmpRMSs.append(totalAmpRMSTmp)    
            totalAmpPaps.append(totalAmpPapTmp)            
        
        f.write('   <hr>\n')
        f.write('   <div style="page-break-after: always"></div>\n')
        f.write('   <big><b>R&eacute;sultats</b></big>\n')
        f.write('   <br>\n')
        f.write('   <b>Tout l\'enregistrement : </b>&eacute;poques ' + str(self.dreamCycles[0].NREMpages[0].stageEpoch)
                  + ' &agrave; ' + str(self.dreamCycles[-1].REMpages[-1].stageEpoch)
                  + ' (' + "%0.1f" % ((self.dreamCycles[-1].REMpages[-1].startTime +  self.dreamCycles[-1].REMpages[-1].timeLength - self.dreamCycles[0].NREMpages[0].startTime)/60.0) + 'minutes)<br>\n')
        f.write('   \n')
        f.write('   <table border="1" cellpadding="0" cellspacing="0"><tbody>\n')
        f.write('       <tr><th style="width: 70px;"></th><th style="text-align: center; width: 86px;">&Eacute;veil</th><th style="text-align: center; width: 86px;">Stade 1</th><th style="text-align: center; width: 86px;">Stade 2</th><th style="text-align: center; width: 86px;">Stade 3</th><th style="text-align: center; width: 86px;">Stade 4</th><th style="text-align: center; width: 86px;">REM</th><th style="text-align: center; width: 86px;">Stade Ind.</th><th style="text-align: center; width: 86px;">Total</th></tr>\n')


        durationString = '       <tr><th>Minutes<sup>2</sup></th>'
        for state in ["WAKE", "Stage1", "Stage2", "Stage3", "Stage4", "REM", "StageU"]:        
            durationString += '<td style="text-align: center;">' + "%0.1f" % duration[state] + '</td>'
        durationString +=  '<td style="text-align: center;">' + "%0.1f" % totalDuration + '</td></tr>\n'
        f.write(durationString)

        numberString = '       <tr><th>Nombre<sup>3</sup></th>'
        for state in ["WAKE", "Stage1", "Stage2", "Stage3", "Stage4", "REM", "StageU"]:        
            numberString += '<td style="text-align: center;">' + str(len(spindleByState[state])) + '</td>'
        numberString +=  '<td style="text-align: center;">' + str(len(spindleEventLst)) + '</td></tr>\n'
        f.write(numberString)

        secondString = '       <tr><th>Secondes<sup>4</sup></th>'
        for state in ["WAKE", "Stage1", "Stage2", "Stage3", "Stage4", "REM", "StageU"]:        
            secondString += '<td style="text-align: center;">' + secondeByState[state] + '</td>'
        secondString +=  '<td style="text-align: center;">' + totalSecond + '</td></tr>\n'
        f.write(secondString)


        frequencyString = '       <tr><th>Fr&eacute;quence<sup>5</sup></th>'
        for state in ["WAKE", "Stage1", "Stage2", "Stage3", "Stage4", "REM", "StageU"]:        
            frequencyString += '<td style="text-align: center;">' +  freqencyByState[state] + '</td>'
        frequencyString +=  '<td style="text-align: center;">' + totalFrequency + '</td></tr>\n'
        f.write(frequencyString)

        ampPaPString = '       <tr><th>Amp&nbsp;P&agrave;P<sup>6</sup></th>'
        for state in ["WAKE", "Stage1", "Stage2", "Stage3", "Stage4", "REM", "StageU"]:        
            ampPaPString += '<td style="text-align: center;">' + ampPaPByState[state] + '</td>'
        ampPaPString +=  '<td style="text-align: center;">' + totalAmpPap + '</td></tr>\n'
        f.write(ampPaPString)

        ampRMSString = '       <tr><th>Amp&nbsp;RMS<sup>7</sup></th>'
        for state in ["WAKE", "Stage1", "Stage2", "Stage3", "Stage4", "REM", "StageU"]:        
            ampRMSString += '<td style="text-align: center;">' + ampRMSByState[state] + '</td>'
        ampRMSString +=  '<td style="text-align: center;">' + totalAmpRMS + '</td></tr>\n'
        f.write(ampRMSString)
        
        f.write('   </tbody></table>\n')
        f.write('   <br>\n')



        
        for cycle, i in zip(self.dreamCycles, range(len(self.dreamCycles))):
            f.write('<br>\n') 
            f.write('<b>Cycle ' + str(i+1) + ' : </b>&eacute;poques ' + str(cycle.NREMpages[0].stageEpoch) + 
                    ' &agrave; ' + str(cycle.REMpages[-1].stageEpoch) + 
                    ' (' + "%0.1f" % ((cycle.REMpages[-1].startTime +  cycle.REMpages[-1].timeLength - cycle.NREMpages[0].startTime)/60.0) + ' minutes)<br>\n') 
            f.write('<b>Seuil du 95<sup>e</sup> percentile : </b>' + 'N/A' + ' &mu;Volts<br>\n') 
            f.write('<table border="1" cellpadding="0" cellspacing="0"><tbody>\n') 
            f.write('    <tr><th style="width: 70px;"></th><th style="text-align: center; width: 86px;">&Eacute;veil</th><th style="text-align: center; width: 86px;">Stade 1</th><th style="text-align: center; width: 86px;">Stade 2</th><th style="text-align: center; width: 86px;">Stade 3</th><th style="text-align: center; width: 86px;">Stade 4</th><th style="text-align: center; width: 86px;">REM</th><th style="text-align: center; width: 86px;">Stade Ind.</th><th style="text-align: center; width: 86px;">Total</th></tr>\n') 
           
            durationString = '       <tr><th>Minutes<sup>2</sup></th>'
            for state in ["WAKE", "Stage1", "Stage2", "Stage3", "Stage4", "REM", "StageU"]:        
                durationString += '<td style="text-align: center;">' + "%0.1f" % durations[i][state] + '</td>'
            durationString +=  '<td style="text-align: center;">' + "%0.1f" % totalDurations[i] + '</td></tr>\n'
            f.write(durationString)           
           

            numberString = '       <tr><th>Nombre<sup>3</sup></th>'
            for state in ["WAKE", "Stage1", "Stage2", "Stage3", "Stage4", "REM", "StageU"]:        
                numberString += '<td style="text-align: center;">' + str(len(spindleByStates[i][state])) + '</td>'
            numberString +=  '<td style="text-align: center;">' + str(len(cycle.Spindles)) + '</td></tr>\n'
            f.write(numberString)
    
            secondString = '       <tr><th>Secondes<sup>4</sup></th>'
            for state in ["WAKE", "Stage1", "Stage2", "Stage3", "Stage4", "REM", "StageU"]:        
                secondString += '<td style="text-align: center;">' + secondeByStates[i][state] + '</td>'
            secondString +=  '<td style="text-align: center;">' + totalSeconds[i] + '</td></tr>\n'
            f.write(secondString)
    
            frequencyString = '       <tr><th>Fr&eacute;quence<sup>5</sup></th>'
            for state in ["WAKE", "Stage1", "Stage2", "Stage3", "Stage4", "REM", "StageU"]:        
                frequencyString += '<td style="text-align: center;">' + freqencyByStates[i][state] + '</td>'
            frequencyString +=  '<td style="text-align: center;">' + totalFrequencies[i] + '</td></tr>\n'
            f.write(frequencyString)
    
            ampPaPString = '       <tr><th>Amp&nbsp;P&agrave;P<sup>6</sup></th>'
            for state in ["WAKE", "Stage1", "Stage2", "Stage3", "Stage4", "REM", "StageU"]:        
                ampPaPString += '<td style="text-align: center;">' + ampPaPByStates[i][state] + '</td>'
            ampPaPString +=  '<td style="text-align: center;">' + totalAmpPaps[i] + '</td></tr>\n'
            f.write(ampPaPString)
    
            ampRMSString = '       <tr><th>Amp&nbsp;RMS<sup>7</sup></th>'
            for state in ["WAKE", "Stage1", "Stage2", "Stage3", "Stage4", "REM", "StageU"]:        
                ampRMSString += '<td style="text-align: center;">' + ampRMSByStates[i][state] + '</td>'
            ampRMSString +=  '<td style="text-align: center;">' + totalAmpRMSs[i] + '</td></tr>\n'
            f.write(ampRMSString)
        

            f.write('</tbody></table>\n') 
            f.write('<br>\n') 


        # Footer
        f.write('<br>\n') 
        f.write('<sup><b>1</b></sup> : L\'amplitude RMS n\'est calcul&eacute;e que pendant ces stades.<br>\n') 
        f.write('<sup><b>2</b></sup> : Dur&eacute;e du stade valide (pendant laquelle la d&eacute;tection a &eacute;t&eacute; effectu&eacute;e), en minutes.<br>\n') 
        f.write('<sup><b>3</b></sup> : Nombre de fuseaux d&eacute;tect&eacute;s.<br>\n') 
        f.write('<sup><b>4</b></sup> : Dur&eacute;e moyenne des fuseaux &plusmn; leur &eacute;cart-type (en secondes).<br>\n') 
        f.write('<sup><b>5</b></sup> : Fr&eacute;quence moyenne du signal filtr&eacute;, en Hz.<br>\n') 
        f.write('<sup><b>6</b></sup> : Amplitude pic &agrave; pic du signal filtr&eacute;, en MicroVolt.<br>\n') 
        f.write('<sup><b>7</b></sup> : Amplitude RMS du signal filtr&eacute;, en MicroVolt.\n') 
        f.write('</body>\n') 
        f.write('</html>\n')       